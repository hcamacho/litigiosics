﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ICS.Litigios.Entities
{
    public class AuthOperation : ISerializable
    {
        public int id { set; get; }
        public string name { set; get; }
        public bool isRestricted { set; get; }
        public List<AuthRole> roles { set; get; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
