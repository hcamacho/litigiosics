﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ICS.Litigios.Entities
{
    public class AuthUser : ISerializable
    {
        public int id { set; get; }
        public string userName { set; get; }
        //TODO: PASSWORD MUST BE CYPHER
        public string password { set; get; }
        public string name { set; get; }
        public string email { set; get; }
        public List<AuthRole> roles { set; get; }
        public List<string> clients { set; get; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            throw new NotImplementedException();
        }
    }
}
