﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ICS.Litigios.Entities
{
    public class Email
    {
        public string to { get; set; }
        public string subject { get; set; }
        public string content { get; set; } 
        public string logoPath { get; set; }

    }
}
