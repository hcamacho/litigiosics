﻿using ICS.Litigios.Entities;
using LitigationICS.BLL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ICS.Litigios.WebApp
{
    public class AuthAttribute : ActionFilterAttribute
    {
        public string Operation { get; set; }
        public bool ClientIdRequired { get; set; }

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            Log("OnActionExecuting", actionContext.RouteData);

            AuthUser user = actionContext.HttpContext.Session["user"] as AuthUser;
            if (user == null){
                actionContext.Result = new RedirectResult("~/Login");
            }
            else{
                string clientId = "";
                if (actionContext.ActionParameters.ContainsKey("clientId")){
                    clientId = Convert.ToString(actionContext.ActionParameters["clientId"]);
                }
                if (ClientIdRequired && string.IsNullOrWhiteSpace(clientId) && user.roles.Exists(r => r.isRestricted)) {
                    actionContext.Result = new HttpStatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);
                } else
                if (!AuthUserLogic.isAuthorized(user.userName, this.Operation, clientId)){
                    actionContext.Result = new RedirectResult("~/Forbidden");
                }
            }
        }

        private void Log(string methodName, RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
            Debug.WriteLine(message, "Action Filter Log");
        }

    }
}