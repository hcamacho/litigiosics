﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using LitigationICS.DAL;
using ICS.Litigios.Entities;
using LitigationICS.BLL;
using System.Web.Mvc;

namespace ICS.Litigios.WebApp.Controllers
{
    public class AdminLitigationController : Controller
    {
        private ClientLogic clientLogic;
        private LitigationLogic litigationLogic;
        private AdminLitigationLogic adminLitigationLogic;
        private JudicialLitigationLogic judicialLitigationLogic;
        private ObservationLogic observationLogic;

        public AdminLitigationController() 
        {
            clientLogic = new ClientLogic();
            litigationLogic = new LitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
            judicialLitigationLogic = new JudicialLitigationLogic();
            observationLogic = new ObservationLogic();
        }

        public ActionResult Create(int? clientId) 
        {
            ViewData["Client"] = clientId;
            ViewData["States"] = adminLitigationLogic.getAdminLitigationState(null);
            ViewData["Types"] = litigationLogic.getLitigationTypeOnDropDown(null);

            return View();
        }

        public ActionResult Delete(int? clientId)
        {
            ViewData["adminLitigations"] = adminLitigationLogic.getListOfAdminLitigationsByClientId(clientId);
            ViewData["client"] = clientLogic.findClientByClientId(clientId);
            return View();
        }

        public ActionResult Edit(int? clientId, int? litigationId) 
        {
            if(litigationId.HasValue){
                litigation litigation = litigationLogic.findLitigationById(litigationId);

                ViewData["recordNumber"] = litigation.recordNumber;
                ViewData["clientId"] = clientId;
                ViewData["States"] = adminLitigationLogic.getAdminLitigationState(litigation.state.State1);
                ViewData["Types"] = litigationLogic.getLitigationTypeOnDropDown(litigation.admin_litigation.Where(al => al.idLitigation == litigation.id).FirstOrDefault().type1.Type1);

                return View(litigationLogic.findLitigationById(litigationId));
            }
            else{
                return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = clientId });
            }
        }

        [HttpPost]
        public ActionResult deleteAdminLitigation(int? clientId, int? adminLitigationId, int? litigationId)
        {
            adminLitigationLogic.getDeleteAdminLitigation(adminLitigationId, litigationLogic);
            litigationLogic.getDeleteClientLitigation(litigationId);
            litigationLogic.getDeleteLitigation(litigationId);
            return RedirectToAction("Delete", "AdminLitigation", new { clientId = clientId });
        }

        public ActionResult detailAdminLitigation(int? adminLitigationId, int? clientId) 
        {
            try{
                if(adminLitigationId.HasValue){
                    setViewValuesAdminLitigationIndex(adminLitigationId, clientId);
                    return View((admin_litigation)ViewData["dataAdminLitigation"]);
                }
                else{
                    return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = clientId });
                }
            }
            catch {
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }

        }

        [HttpPost] 
        public ActionResult AdminLitigationRegistered(string recordNumber, string transferCharges, long? estimate, string coin, bool? unrated, string fee, string drpTypes, string taxAdmin, string objectProcess, DateTime initDate, string drpStates, string nextStepDetail, DateTime? nextStepDate, int? clientId)
        {
            litigation litigation = new litigation() { 
                recordNumber = recordNumber, 
                estimate = litigationLogic.getEstimate(unrated, estimate), 
                coin = coin, unrated = litigationLogic.getUnrated(unrated), 
                fee = fee, initDate = initDate, 
                actualState = drpStates, 
                nextStepDetail = nextStepDetail, 
                nextStepDate = nextStepDate, 
                objectProcess = objectProcess, 
                type = 2 
            };
            
            admin_litigation adminLitigation = new admin_litigation { 
                transferCharges = transferCharges, 
                TaxAdmin = taxAdmin, 
                Type = drpTypes, 
                litigation = litigation 
            };

            int? lastIdInserted = litigationLogic.registerNewAdminLitigation(adminLitigation, int.Parse(clientId.ToString()));
            return RedirectToAction("detailAdminLitigation", "AdminLitigation", new { adminLitigationId = lastIdInserted, clientId = clientId });
        }

        private List<client> getListOfClientsWithLitigationsByClientId(int? clientId)
        {
            List<client> clients = new List<client>();
            AuthUser authUser = (AuthUser)Session["user"];

            if (clientId == null || authUser.roles.FirstOrDefault().name.Equals("administrator")){
                clients = clientLogic.getListOfClientsWithLitigation();
            }
            else{
                clients = clientLogic.getListOfClientsWithLitigationByClientId(clientId);
            }
            return clients;
        }

        private void setViewValuesAdminLitigationIndex(int? adminLitigationId, int? clientId)
        {
            ViewData["clientId"] = clientId;
            ViewData["litigationId"] = litigationLogic.getLitigationId(0, adminLitigationId);
            ViewData["dataAdminLitigation"] = adminLitigationLogic.getAdminLitigationByAdminLitigationId(adminLitigationId, litigationLogic);
            ViewData["listClients"] = getListOfClientsWithLitigationsByClientId(clientId);
            ViewData["listJudicialLitigations"] = judicialLitigationLogic.getListOfJudicialLitigationsByClientId(clientId);
            ViewData["listAdminLitigations"] = adminLitigationLogic.getListOfAdminLitigationsByClientId(clientId);
            ViewData["listObservations"] = observationLogic.getListOfObservationsFromAdminLitigation(adminLitigationId);
        }
    }
}
