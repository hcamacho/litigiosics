﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LitigationICS.DAL;
using LitigationICS.BLL;
using ICS.Litigios.Entities;

namespace ICS.Litigios.WebApp.Controllers
{
    public class ClientController : Controller
    {
        private ClientLogic clientLogic;
        private LitigationLogic litigationLogic;
        private JudicialLitigationLogic judicialLitigationLogic;
        private AdminLitigationLogic adminLitigationLogic;
        private LegalRepresentativeLogic legalRepresentativeLogic;
        private PersonLogic personLogic;
        private ContactLogic contactLogic;

        public ClientController()
        {
            clientLogic = new ClientLogic();
            litigationLogic = new LitigationLogic();
            judicialLitigationLogic = new JudicialLitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
            legalRepresentativeLogic = new LegalRepresentativeLogic();
            personLogic = new PersonLogic();
            contactLogic = new ContactLogic();
        }

        public ActionResult Index()
        {
            ViewData["listClients"] = clientLogic.getListOfClientsWithLitigation();
            return View();
        }

        [Auth(Operation = "CLIENT.ADD", ClientIdRequired = false)]
        public ActionResult Create()
        {
            return View();
        }
                
        [Auth(Operation = "CLIENT.EDIT", ClientIdRequired = true)]
        public ActionResult editClient(int? clientId) 
        {
            try{
                ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId);
                return View(clientLogic.findClientByClientId(clientId));
            }
            catch{
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CLIENT.REPORT", ClientIdRequired = true)]
        public ActionResult clientReport(int? clientId)
        {
            try {
                List<admin_litigation> listAdminLitigations = adminLitigationLogic.getListOfAdminLitigationsByClientId(clientId);
                List<juditial_litigation> listJudicialLitigations = judicialLitigationLogic.getListOfJudicialLitigationsByClientId(clientId); 
                client client = clientLogic.getClientIdAndNameByClientId(clientId);

                if (listAdminLitigations != null)
                    ViewData["adminLitigations"] = listAdminLitigations;
                if (listJudicialLitigations != null)
                    ViewData["judicialLitigations"] = listJudicialLitigations;
                if (client != null)
                    ViewData["client"] = client;
                else
                    return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                return View();
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CLIENT.EDIT", ClientIdRequired = true)]
        public ActionResult editClientWithoutContact(int? clientId) 
        {
            try {
                ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId);
                return View(clientLogic.findClientByClientId(clientId));
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CLIENT.VIEW", ClientIdRequired = true)]
        public ActionResult detailClients(int? clientId) 
        {
            try {
                List<client> clients = new List<client>();
                clients = getListOfClientsByClientId(clientId, clients);
                ViewData["listClients"] = clients;
                return View();
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CLIENT.VIEW", ClientIdRequired = true)]
        public ActionResult showClientInformation(int? clientId)
        {
            try{
                if(clientId.HasValue){
                    List<client> clients = new List<client>();
                    client clientSelected = clientLogic.findClientByClientId(clientId);

                    clients = getListOfClientsByClientId(clientId, clients);
                    ViewData["listClients"] = clients;
                    ViewData["listLitigations"] = litigationLogic.getListOfLitigationsWithClientByClientId(clientId);
                    ViewData["dropDownClientList"] = clientLogic.getListOfPersonsNameForDropDown();
                    ViewData["clientSelected"] = clientSelected;

                    if (contactLogic.getClientContactByClientId(clientId) == null)
                        return View("showClientInformationWithoutContact", new { clientId = clientId });
                    return View();
                }
                else{
                    return RedirectToAction("detailClients", "Client", new { clientId = clientId });
                }
            }
            catch{
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CLIENT.VIEW", ClientIdRequired = true)]
        public ActionResult showClientInformationWithoutContact(int? clientId) 
        {
            try{
                if(clientId.HasValue){
                    List<client> clients = new List<client>();

                    clients = getListOfClientsByClientId(clientId, clients);
                    ViewData["listClients"] = clients;
                    ViewData["listLitigations"] = litigationLogic.getListOfLitigationsWithClientByClientId(clientId); ;
                    ViewData["dropDownClientList"] = clientLogic.getListOfPersonsNameForDropDown();
                    ViewData["clientSelected"] = clientLogic.findClientByClientId(clientId);
                    return View();
                }
                else{
                    return RedirectToAction("detailClients", "Client", new { clientId = clientId });
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        public ActionResult registerNewClient()
        {
            int lastClientId = registerClientDependencies();

            if (lastClientId > 0)
                return RedirectToAction("showClientInformation", "Client", new { clientId = lastClientId });
            else{
                ViewData["failedRegisterClient"] = IConstants.failedRegisterClient;
                ViewData["observationsByFailed"] = IConstants.getListOfObservations();
                return View("Create");
            }
        }

        [HttpPost]
        public ActionResult updateClient(int? clientId, 
                                         int? contactId, 
                                         int clientPersonId, 
                                         int? contactPersonId, 
                                         int? legalRepresentativePersonId,
                                         int? legalRepresentativeId, 
                                         int? phoneId, int? emailId) {
            if (contactId != null){
                return updateClientWithContact(clientId, contactId, clientPersonId, contactPersonId, legalRepresentativePersonId, legalRepresentativeId, phoneId, emailId);
            }
            else{
                return updateClientAndRegisterContact(clientId, clientPersonId, legalRepresentativePersonId, legalRepresentativeId);
            }
        }

        private List<client> getListOfClientsByClientId(int? clientId, List<client> clients)
        {
            AuthUser authUser = (AuthUser)Session["user"];

            if (clientId == null || authUser.roles.FirstOrDefault().name.Equals("administrator")){
                clients = clientLogic.getListOfClients();
            }
            else{
                clients.Add(clientLogic.findClientByClientId(clientId));
            }
            return clients;
        }

        private ActionResult updateClientAndRegisterContact(int? clientId, 
                                                            int clientPersonId, 
                                                            int? legalRepresentativePersonId,
                                                            int? legalRepresentativeId) {
            int lastPersonId = 0;
            int lastContactId = 0;
            client clientUpdated = clientLogic.findClientByClientId(clientId);
            person clientPersonUpdated = clientLogic.findPersonByPersonId(clientPersonId);
            legalrepresentative legalRepresentativeUpdated = clientLogic.findLegalRepresentativeByLegalRepresentativeId(legalRepresentativeId);
            person legalRepresentativePersonUpdated = clientLogic.findPersonByPersonId(legalRepresentativePersonId);

            if (!String.IsNullOrEmpty(Request.Form["contactName"]) && !String.IsNullOrEmpty(Request.Form["contactPhone"]) && !String.IsNullOrEmpty(Request.Form["contactEmail"])){
                lastPersonId = registerNewContactPerson(false);

                if (lastPersonId > 0){
                    lastContactId = registerNewContact(lastPersonId);
                    if(lastContactId != 0 && clientId != 0){
                        contactLogic.registerNewClientContact(lastContactId, clientId);
                    }
                }
            }

            updateClient(clientPersonId, clientUpdated, clientPersonUpdated, legalRepresentativeUpdated, legalRepresentativePersonUpdated);
            return RedirectToAction("showClientInformation", "Client", new { clientId = clientId });
        }

        private ActionResult updateClientWithContact(int? clientId,
                                                     int? contactId,
                                                     int clientPersonId,
                                                     int? contactPersonId,
                                                     int? legalRepresentativePersonId,
                                                     int? legalRepresentativeId,
                                                     int? phoneId, int? emailId) {
            client clientUpdated = clientLogic.findClientByClientId(clientId);
            person clientPersonUpdated = clientLogic.findPersonByPersonId(clientPersonId);
            contact contactUpdated = clientLogic.findContactByContactId(contactId);
            person contactPersonUpdated = clientLogic.findPersonByPersonId(contactPersonId);
            legalrepresentative legalRepresentativeUpdated = clientLogic.findLegalRepresentativeByLegalRepresentativeId(legalRepresentativeId);
            person legalRepresentativePersonUpdated = clientLogic.findPersonByPersonId(legalRepresentativePersonId);
            user_email contactEmail = clientLogic.findEmailByEmailId(emailId);
            user_phone contactPhone = clientLogic.findPhoneByPhoneId(phoneId);

            if (contactEmail == null && contactPhone == null){
                personLogic.updateNewContactPhoneAndEmail(Request.Form["contactEmail"], Request.Form["contactPhone"], clientPersonId, contactEmail, contactPhone);
            }

            updateClient(clientPersonId, clientUpdated, clientPersonUpdated, legalRepresentativeUpdated, legalRepresentativePersonUpdated);
            updateContact(contactUpdated, contactPersonUpdated, contactEmail, contactPhone);
            return RedirectToAction("showClientInformation", "Client", new { clientId = clientId });
        }


        private void updateClient(int? clientPersonId, 
                                  client clientUpdated, 
                                  person clientPersonUpdated, 
                                  legalrepresentative legalRepresentativeUpdated, 
                                  person legalRepresentativePersonUpdated) {
            legalRepresentativePersonUpdated.Name = Request.Form["clientLegalRepresentative"];
            legalRepresentativePersonUpdated.DNI = Request.Form["clientLegalRepresentativeDNI"];
            legalRepresentativeUpdated.person = legalRepresentativePersonUpdated;
            clientPersonUpdated.Name = Request.Form["clientName"];
            clientPersonUpdated.DNI = Request.Form["clientDNI"];
            clientPersonUpdated.Address = Request.Form["clientAddress"];
            clientUpdated.person = clientPersonUpdated;
            clientUpdated.idPerson = clientPersonId;
            clientLogic.updateClient(clientUpdated);
        }

        private void updateContact(contact contactUpdated, 
                                   person contactPersonUpdated, 
                                   user_email contactEmail, 
                                   user_phone contactPhone) {
            if(!String.IsNullOrEmpty(Request.Form["contactEmail"]) && !String.IsNullOrEmpty(Request.Form["contactPhone"])){
                contactEmail.Email = Request.Form["contactEmail"];
                contactPhone.Phone = Request.Form["contactPhone"];
                contactPersonUpdated.Name = Request.Form["contactName"];
                contactPersonUpdated.LastName = Request.Form["contactLastName"];
                contactPersonUpdated.DNI = Request.Form["contactDNI"];
                contactUpdated.Position = Request.Form["contactPosition"];
                contactUpdated.person = contactPersonUpdated;
                clientLogic.updateContact(contactPersonUpdated, contactUpdated, contactEmail, contactPhone);
            }
        }

        private int registerClientDependencies()
        {
            int clientId = 0;
            int contactId = 0;
            int personId = 0;
            int legalRepresentativeId = 0;

            if (!String.IsNullOrEmpty(Request.Form["contactName"]) 
                && !String.IsNullOrEmpty(Request.Form["contactEmail"])
                && !String.IsNullOrEmpty(Request.Form["contactPhone"]))
            {
                personId = registerNewContactPerson(true);
                if (personId > 0){
                    contactId = registerNewContact(personId);
                }
            }

            personId = registerNewLegalRepresentativePerson();
            if(personId > 0){
                legalRepresentativeId = registerNewLegalRepresentative(personId);
            }

            personId = registerNewClientPerson();
            if(personId > 0){
                clientId = registerNewClient(legalRepresentativeId,personId, contactId);
                if(clientId != 0 && contactId != 0){
                    contactLogic.registerNewClientContact(contactId, clientId);
                }
            }
            return clientId;
        }

        private int registerNewClient(int legalRepresentativeId, int personId, int contactId)
        {
            int lastClientId = clientLogic.getRegisterNewClient(legalRepresentativeId, personId, contactId);
            return lastClientId;
        }

        private int registerNewClientPerson()
        {
            int personId = personLogic.registerNewPerson(Request.Form["clientName"], Request.Form["clientDNI"], Request.Form["clientAddress"], null, null);
            return personId;
        }

        private int registerNewContact(int? personId)
        {
            int contactId = contactLogic.registerNewContact(Request.Form["contactPosition"], personId);
            return contactId;
        }

        private int registerNewContactPerson(bool isForEdit)
        {
            int personId = personLogic.registerNewContactPerson(Request.Form["clientName"], Request.Form["contactName"], Request.Form["contactLastName"], null, null, Request.Form["contactEmail"], Request.Form["contactPhone"], isForEdit);
            return personId;
        }

        private int registerNewLegalRepresentative(int? personId) 
        {
            int legalRepresentativeId = legalRepresentativeLogic.registerNewLegalRepresentative(personId);
            return legalRepresentativeId;
        }

        private int registerNewLegalRepresentativePerson() 
        {
            int personId = personLogic.registerNewLegalRepresentativePerson(Request.Form["legalRepresentativeName"], Request.Form["legalRepresentativeNameDNI"]);
            return personId;
        }
    }
}
