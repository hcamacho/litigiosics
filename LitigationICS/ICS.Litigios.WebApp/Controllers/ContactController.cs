﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LitigationICS.BLL;
using LitigationICS.DAL;
using ICS.Litigios.Entities;

namespace ICS.Litigios.WebApp.Controllers
{
    public class ContactController : Controller
    {
        private ClientLogic clientLogic;
        private ContactLogic contactLogic;
        private PersonLogic personLogic;

        public ContactController() 
        {
            clientLogic = new ClientLogic();
            contactLogic = new ContactLogic();
            personLogic = new PersonLogic();
        }

        [Auth(Operation = "CONTACT.VIEW", ClientIdRequired = true)]
        public ActionResult Index(int? clientId)
        {
            try{
                List<client> clients = new List<client>();
                clients = getListOfClientsByClientId(clientId, clients);
                ViewData["listClients"] = clients;
                return View();
            }
            catch{
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CONTACT.ADD", ClientIdRequired = true)]
        public ActionResult Create(int? clientId, bool? errorMessage) 
        {
            if(clientId.HasValue){
                client clientSelected = clientLogic.findClientByClientId(clientId);
                ViewData["clientSelected"] = clientSelected;
                if (errorMessage.Value)
                    ViewData["failedRegisterContact"] = IConstants.failedRegisterContact;
                return View();
            }
            else{
                return RedirectToAction("Index", "Contact", new { clientId = clientId });
            }
        }

        [HttpPost]
        public ActionResult registerContact(int? clientId)
        {
            int? lastContactId = registerContactDependencies(clientId);
            if (lastContactId > 0)
                return RedirectToAction("showContact", "Contact", new { clientId = clientId, contactId = lastContactId });
            else
            {
                bool? errorMessage = true;
                return RedirectToAction("Create", "Contact", new { clientId = clientId, errorMessage = errorMessage });
            }
        }

        [Auth(Operation = "CONTACT.VIEW", ClientIdRequired = true)]
        public ActionResult showContacts(int? clientId)
        {
            try{
                if(clientId.HasValue){
                    List<client> clients = new List<client>();
                    clients = getListOfClientsByClientId(clientId, clients);
                    client clientSelected = clientLogic.findClientByClientId(clientId);
                    ViewData["listClients"] = clients;
                    ViewData["listContacts"] = contactLogic.getListOfContactsByClientId(clientId);
                    ViewData["clientSelected"] = clientSelected;

                    if (contactLogic.getClientContactByClientId(clientId) == null)
                        ViewData["clientWithoutContact"] = IConstants.clientWithoutContact;
                    return View();
                }
                else{
                    return RedirectToAction("Index", "Contact", new { clientId = clientId });
                }
            }
            catch{
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "CONTACT.VIEW", ClientIdRequired = true)]
        public ActionResult showContact(int? clientId, int? contactId)
        {
            try{
                if(clientId.HasValue){
                    List<client> clients = new List<client>();
                    clients = getListOfClientsByClientId(clientId, clients);
                    contact contactSelected = contactLogic.findContactByContactId(contactId);
                    client clientSelected = clientLogic.findClientByClientId(clientId);

                    ViewData["listClients"] = clients;
                    ViewData["contactSelected"] = contactSelected;
                    ViewData["clientSelected"] = clientSelected;
                    return View();
                }
                else{
                    return RedirectToAction("Index", "Contact", new { clientId = clientId });
                }

            }
            catch{
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        private int registerNewContact(int? personId)
        {
            int contactId = contactLogic.registerNewContact(Request.Form["contactPosition"], personId);
            return contactId;
        }

        private int registerNewContactPerson(bool isForEdit)
        {
            int personId = personLogic.registerNewContactPerson(Request.Form["clientName"], Request.Form["contactName"], Request.Form["contactLastName"], null, null, Request.Form["contactEmail"], Request.Form["contactPhone"], isForEdit);
            return personId;
        }

        private int? registerContactDependencies(int? clientId)
        {
            int? contactId = 0;
            int? personId = 0;

            if(!String.IsNullOrEmpty(Request.Form["contactEmail"]) && !String.IsNullOrEmpty(Request.Form["contactPhone"])){
                if (!String.IsNullOrEmpty(Request.Form["contactName"])){
                    personId = registerNewContactPerson(true);
                    if (personId > 0){
                        contactId = registerNewContact(personId);
                        if(contactId != 0 && clientId != 0){
                            contactLogic.registerNewClientContact(contactId, clientId);
                        }
                    }
                }
            }
            return contactId;
        }

        private List<client> getListOfClientsByClientId(int? clientId, List<client> clients)
        {
            AuthUser authUser = (AuthUser)Session["user"];

            if (clientId == null || authUser.roles.FirstOrDefault().name.Equals("administrator")){
                clients = clientLogic.getListOfClients();
            }
            else{
                clients.Add(clientLogic.findClientByClientId(clientId));
            }
            return clients;
        }
	}
}