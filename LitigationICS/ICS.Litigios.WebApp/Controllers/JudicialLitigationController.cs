﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ICS.Litigios.Entities;
using LitigationICS.DAL;
using LitigationICS.BLL;

namespace ICS.Litigios.WebApp.Controllers
{
    public class JudicialLitigationController : Controller
    {        
        private ClientLogic clientLogic;
        private LitigationLogic litigationLogic;
        private AdminLitigationLogic adminLitigationLogic;
        private JudicialLitigationLogic judicialLitigationLogic;
        private ObservationLogic observationLogic;

        public JudicialLitigationController()
        {
            clientLogic = new ClientLogic();
            litigationLogic = new LitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
            judicialLitigationLogic = new JudicialLitigationLogic();
            observationLogic = new ObservationLogic();
        }

        public ActionResult Index(int? judicialLitigationId, int? clientId)
        {
            try{
                if(judicialLitigationId.HasValue){
                    setViewValuesJudicialLitigationIndex(judicialLitigationId, clientId);
                    return View((juditial_litigation)ViewData["dataJudicialLitigation"]);
                }
                else{
                    return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = clientId });
                }
            }
            catch(Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        public ActionResult Create(int? clientId) 
        {
            ViewData["Client"] = clientId;
            ViewData["States"] = judicialLitigationLogic.getJudicialLitigationState(null);
            ViewData["cautionStates"] = judicialLitigationLogic.getJudicialLitigationCautionState(null);
            ViewData["Types"] = litigationLogic.getLitigationTypeOnDropDown(null);
            return View();
        }

        public ActionResult Delete(int? clientId)
        {
            ViewData["judicialLitigations"] = judicialLitigationLogic.getListOfJudicialLitigationsByClientId(clientId);
            ViewData["client"] = clientLogic.findClientByClientId(clientId);
            return View();
        }

        public ActionResult Edit(int? clientId, int? litigationId)
        {
            litigation litigation = litigationLogic.findLitigationById(litigationId);
            ViewData["recordNumber"] = litigation.recordNumber;
            ViewData["clientId"] = clientId;
            ViewData["States"] = judicialLitigationLogic.getJudicialLitigationState(litigation.state.State1);
            ViewData["cautionStates"] = judicialLitigationLogic.getJudicialLitigationCautionState(litigation.juditial_litigation.SingleOrDefault(jl => jl.litigation.id == litigationId).cautionActualState);
            ViewData["Types"] = litigationLogic.getLitigationTypeOnDropDown(null);
            return View(litigationLogic.findLitigationById(litigationId));
        }

        [HttpPost]
        public ActionResult deleteJudicialLitigation(int? clientId, int? judicialLitigationId, int? litigationId)
        {
            judicialLitigationLogic.getDeleteJudicialLitigation(judicialLitigationId, litigationLogic);
            litigationLogic.getDeleteClientLitigation(litigationId);
            litigationLogic.getDeleteLitigation(litigationId);
            return RedirectToAction("Delete", "JudicialLitigation", new { clientId = clientId });
        }

        [HttpPost]
        public ActionResult JudicialLitigationRegistered(string recordNumber, long? estimate, string coin, bool? unrated, string fee, string court, string actor, string defendant, string adjuvant, string objectProcess, DateTime initDate, string drpStates, string nextStepDetail, DateTime? nextStepDate, string drpStatesCaution, string cautionNextStepDetail, DateTime? cautionNextStepDate, int? clientId)
        {
            litigation litigation = new litigation() { 
                recordNumber = recordNumber, 
                estimate = litigationLogic.getEstimate(unrated, estimate), 
                coin = coin, unrated = litigationLogic.getUnrated(unrated), 
                fee = fee,
                initDate = initDate, 
                actualState = drpStates, 
                nextStepDetail = nextStepDetail, 
                nextStepDate = nextStepDate, 
                objectProcess = objectProcess, 
                type = 1 
            };

            juditial_litigation judicialLitigation = new juditial_litigation { 
                litigation = litigation, 
                Court = court, 
                Actor = actor, 
                Defendant = defendant, 
                Adjuvant = adjuvant, 
                cautionActualState = drpStatesCaution, 
                cautionNextStepDate = cautionNextStepDate, 
                cautionNextStepDetail = cautionNextStepDetail 
            };
            
            int? lastIdInserted = litigationLogic.registerNewJudicialLitigation(judicialLitigation, int.Parse(clientId.ToString()));
            return RedirectToAction("Index", "JudicialLitigation", new { judicialLitigationId = lastIdInserted, clientId = clientId });   
        }

        private List<client> getListOfClientsWithLitigationsByClientId(int? clientId)
        {
            List<client> clients = new List<client>();
            AuthUser authUser = (AuthUser)Session["user"];

            if (clientId == null || authUser.roles.FirstOrDefault().name.Equals("administrator")){
                clients = clientLogic.getListOfClientsWithLitigation();
            }
            else{
                clients = clientLogic.getListOfClientsWithLitigationByClientId(clientId);
            }
            return clients;
        }

        private void setViewValuesJudicialLitigationIndex(int? judicialLitigationId, int? clientId)
        {
            ViewData["clientId"] = clientId;
            ViewData["litigationId"] = litigationLogic.getLitigationId(judicialLitigationId, 0);
            ViewData["dataJudicialLitigation"] = judicialLitigationLogic.getJudicialLitigationByJudicialLitigationId(judicialLitigationId, litigationLogic);
            ViewData["listClients"] = getListOfClientsWithLitigationsByClientId(clientId);
            ViewData["listJudicialLitigations"] = judicialLitigationLogic.getListOfJudicialLitigationsByClientId(clientId);
            ViewData["listAdminLitigations"] = adminLitigationLogic.getListOfAdminLitigationsByClientId(clientId);
            ViewData["listObservations"] = observationLogic.getListOfObservationsFromJudicialLitigation(judicialLitigationId);
        }

    }
}
