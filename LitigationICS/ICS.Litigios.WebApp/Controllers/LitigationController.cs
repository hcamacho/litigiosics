﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LitigationICS.DAL;
using LitigationICS.BLL;
using ICS.Litigios.Entities;

namespace ICS.Litigios.WebApp.Controllers
{
    public class LitigationController : Controller
    {
        private ClientLogic clientLogic;
        private LitigationLogic litigationLogic;
        private JudicialLitigationLogic judicialLitigationLogic;
        private AdminLitigationLogic adminLitigationLogic;
        private ObservationLogic observationLogic;

        public LitigationController()
        {
            litigationLogic = new LitigationLogic();
            judicialLitigationLogic = new JudicialLitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
            clientLogic = new ClientLogic();
            observationLogic = new ObservationLogic();
        }

        [Auth(Operation = "LITIGATION.VIEW", ClientIdRequired=true)]
        public ActionResult Index(int? clientId, bool? isTheMainPage)
        {
            try
            {
                if (litigationLogic.exitSomeClientWithLitigations()){
                    if (isTheMainPage.Value && clientId == null){
                        isMainPageLitigation(clientId);
                    }
                    else{
                        notIsMainPageLitigation(clientId);
                    }
                    return View();
                }
                else if (clientLogic.exitSomeClient()){
                    return RedirectToAction("detailClients", "Client");
                }
                return RedirectToAction("Create", "Client");
            }
            catch{
                return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = clientId });
            }
        }

        [HttpPost]
        [Auth(Operation = "LITIGATION.EDIT", ClientIdRequired = true)]
        public ActionResult Edit(int? clientId, int? litigationId, int? litigationType)
        {
            if(litigationType == 1){
                return RedirectToAction("Edit", "JudicialLitigation", new { clientId = clientId, litigationId = litigationId });
            }
            else {
                return RedirectToAction("Edit", "AdminLitigation", new { clientId = clientId, litigationId = litigationId });
            }
        }

        [HttpPost]
        [Auth(Operation = "LITIGATION.DELETE", ClientIdRequired = true)]
        public ActionResult Delete(string rdbLitigation, int? clientId)
        {
            if (int.Parse(rdbLitigation) == 1){
                return RedirectToAction("Delete", "JudicialLitigation", new { clientId = clientId });
            }
            else{
                return RedirectToAction("Delete", "AdminLitigation", new { clientId = clientId });
            }
        }

        [HttpPost]
        public ActionResult updateLitigation(int? clientId, int? litigationId, bool? litigationType)
        {
            if(litigationType == true){
                litigation litigationUpdated = litigationLogic.findLitigationById(litigationId);
                juditial_litigation jLitigationUpdated = judicialLitigationLogic.findJudicialLitigationByLitigationId(litigationId);
                updateLitigationByObject(clientId, litigationId, null, jLitigationUpdated, litigationUpdated, true);
                return RedirectToAction("Index", "JudicialLitigation", new { judicialLitigationId = judicialLitigationLogic.getJudicialLitigationIdByLitigationId(litigationId), clientId = clientId });
            }
            else{
                litigation litigationUpdated = litigationLogic.findLitigationById(litigationId);
                admin_litigation aLitigationUpdated = adminLitigationLogic.findAdminLitigationByLitigationId(litigationId);
                updateLitigationByObject(clientId, litigationId, aLitigationUpdated, null, litigationUpdated, false);
                return RedirectToAction("detailAdminLitigation", "AdminLitigation", new { adminLitigationId = adminLitigationLogic.getAdminLitigationIdByLitigationId(litigationId), clientId = clientId });
            }
        }

        [Auth(Operation = "LITIGATION.ADD", ClientIdRequired = true)]
        public ActionResult CreateNewLitigation(string rdbLitigation, int? clientId) 
        {
            if(int.Parse(rdbLitigation) == 1){
                return RedirectToAction("Create", "JudicialLitigation", new { clientId = clientId });
            }
            else{
                return RedirectToAction("Create", "AdminLitigation", new { clientId = clientId });
            }
        }

        public ActionResult backToLitigation(int? litigationId, int? clientId)
        {
            byte? litigationType = litigationLogic.getLitigationTypeByLitigationId(litigationId);

            ViewData["litigationId"] = litigationId;
            ViewData["clientId"] = clientId;
            ViewData["type"] = litigationType;

            if (litigationType == 1){
                int? judicialLitigationId = judicialLitigationLogic.getJudicialLitigationIdByLitigationId(litigationId);
                return RedirectToAction("Index", "JudicialLitigation", new { judicialLitigationId = judicialLitigationId, clientId = clientId });
            }
            else{
                int? adminLitigationId = adminLitigationLogic.getAdminLitigationIdByLitigationId(litigationId);
                return RedirectToAction("detailAdminLitigation", "AdminLitigation", new { adminLitigationId = adminLitigationId, clientId = clientId });
            }

        }

        public ActionResult selectJudicialOrAdminLitigation(int type, int? litigationId, int? clientId)
        {
            int? judicialOrAdminLitigationId = litigationLogic.getJudicialOrAdminLitigationId(type, litigationId);

            if (type == 1){
                return RedirectToAction("Index", "JudicialLitigation", new { judicialLitigationId = judicialOrAdminLitigationId, clientId = clientId });
            }
            return RedirectToAction("detailAdminLitigation", "AdminLitigation", new { adminLitigationId = judicialOrAdminLitigationId, clientId = clientId });
        }

        private bool isMainPageLitigation(int? clientId)
        {
            int lastClientRegistratedWithLitigation = clientLogic.getLastClientIdRegistratedWithLitigation();

            ViewData["clientId"] = lastClientRegistratedWithLitigation;
            ViewData["listClients"] = getListOfClientsValidatedWithIdAndNameNullable(clientId);
            ViewData["listJudicialLitigations"] = judicialLitigationLogic.getListOfJudicialLitigationsByClientId(lastClientRegistratedWithLitigation);
            ViewData["listAdminLitigations"] = adminLitigationLogic.getListOfAdminLitigationsByClientId(clientLogic.getLastClientIdRegistratedWithLitigation());
            return true;
        }

        private bool notIsMainPageLitigation(int? clientId)
        {
            ViewData["clientId"] = clientId;
            ViewData["listClients"] = getListOfClientsValidatedWithIdAndNameNullable(clientId);
            ViewData["listJudicialLitigations"] = judicialLitigationLogic.getListOfJudicialLitigationsByClientId(clientId);
            ViewData["listAdminLitigations"] = adminLitigationLogic.getListOfAdminLitigationsByClientId(clientId);
            return true;
        }

        private void updateLitigationByObject(int? clientId, 
                                              int? litigationId, 
                                              admin_litigation aLitigation, 
                                              juditial_litigation jLitigation, 
                                              litigation litigation, 
                                              bool? litigationType) {
            if (litigationType == true){
                modifyJudicialLitigationValues(jLitigation, litigation);
                litigationLogic.updateLitigationByObject(null, jLitigation, true);
            }
            else{
                modifyAdminLitigationValues(aLitigation, litigation);
                litigationLogic.updateLitigationByObject(aLitigation, null, false);
            }
        }

        private List<client> getListOfClientsValidatedWithIdAndNameNullable(int? clientId)
        {
            List<client> clients = new List<client>();
            AuthUser authUser = (AuthUser)Session["user"];

            if (clientId == null || authUser.roles.FirstOrDefault().name.Equals("administrator")){
                clients = clientLogic.getListOfClientsWithLitigation();
            }
            else{
                clients = clientLogic.getListOfClientsWithLitigationByClientId(clientId);
            }
            return clients;
        }

        private void modifyAdminLitigationValues(admin_litigation aLitigation, litigation litigation)
        {
            litigation.recordNumber = Request.Form["recordNumber"];
            litigation.fee = Request.Form["adminFee"];
            aLitigation.TaxAdmin = Request.Form["adminTaxAdmin"];
            aLitigation.transferCharges = Request.Form["transferCharges"];
            aLitigation.Type = Request.Form["adminType"];
            litigation.objectProcess = Request.Form["adminObjectProcess"];
            litigation.actualState = Request.Form["adminActualState"];
            litigation.initDate = DateTime.Parse(Request.Form["initDate"]);
            litigation.nextStepDetail = Request.Form["adminNextStepDetail"];

            if (!String.IsNullOrEmpty(Request.Form["adminNextStepDate"])){
                litigation.nextStepDate = DateTime.Parse(Request.Form["adminNextStepDate"]);
            }

            setAndValidateEstimation(litigation, "adminEstimate");
            aLitigation.litigation = litigation;
        }

        private void modifyJudicialLitigationValues(juditial_litigation jLitigation, litigation litigation)
        {
            litigation.recordNumber = Request.Form["recordNumber"];
            litigation.fee = Request.Form["judicialFee"];
            jLitigation.Actor = Request.Form["Actor"];
            jLitigation.Defendant = Request.Form["Defendant"];
            jLitigation.Adjuvant = Request.Form["judicialAdjuvant"];
            jLitigation.Court = Request.Form["Court"];
            litigation.objectProcess = Request.Form["judicialObjectProcess"];
            litigation.actualState = Request.Form["judicialActualState"];
            litigation.initDate = DateTime.Parse(Request.Form["initDate"]);
            litigation.nextStepDetail = Request.Form["nextStepDetail"];

            if(!String.IsNullOrEmpty(Request.Form["nextStepDate"])){
                litigation.nextStepDate = DateTime.Parse(Request.Form["nextStepDate"]);
            }

            jLitigation.cautionActualState = Request.Form["judicialCautionActualState"];
            jLitigation.cautionNextStepDetail = Request.Form["cautionNextStepDetail"];
            setAndValidateEstimation(litigation, "judicialEstimate");
            jLitigation.litigation = litigation;

            if (!String.IsNullOrEmpty(Request.Form["cautionNextStepDate"])){
                jLitigation.cautionNextStepDate = DateTime.Parse(Request.Form["cautionNextStepDate"]);
            }
        }

        private void setAndValidateEstimation(litigation litigation, string requestIdentifier) 
        {
            if (String.IsNullOrWhiteSpace(Request.Form[requestIdentifier])){
                litigation.estimate = null;
                litigation.coin = Request.Form["coin"];
                litigation.unrated = true;
            }
            else{
                litigation.coin = Request.Form["coin"];
                litigation.estimate = long.Parse(Request.Form[requestIdentifier]);
                litigation.unrated = false;
            }
        }
    }
}
