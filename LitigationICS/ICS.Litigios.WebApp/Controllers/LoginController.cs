﻿using ICS.Litigios.Entities;
using LitigationICS.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ICS.Litigios.WebApp.Controllers
{
    public class LoginController : Controller
    {
        private ClientLogic clientLogic;

        public LoginController() 
        {
            clientLogic = new ClientLogic();
        }

        public ActionResult Index()
        {
            int lastClientRegistratedWithLitigation = clientLogic.getLastClientIdRegistratedWithLitigation();
            return View();
        }

        [HttpPost]
        public ActionResult Login(String userName, String userPassword)
        {
            AuthUser user = AuthUserLogic.authenticateUser(userName, userPassword);
            if (user == null){
                ModelState.AddModelError("loginError","Usuario o contraseña incorrectas");
                return View("Index");
            }
            else{
                this.HttpContext.Session.Add("user", user);
                return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = user.clients.FirstOrDefault() });
            }
        }

    }
}
