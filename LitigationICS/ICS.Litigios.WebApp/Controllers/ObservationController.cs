﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LitigationICS.DAL;
using LitigationICS.BLL;

namespace ICS.Litigios.WebApp.Controllers
{
    public class ObservationController : Controller
    {
        private ObservationLogic observationLogic;
        private ClientLogic clientLogic;
        private LitigationLogic litigationLogic;
        private JudicialLitigationLogic judicialLitigationLogic;
        private AdminLitigationLogic adminLitigationLogic;

        public ObservationController()
        {
            observationLogic = new ObservationLogic();
            clientLogic = new ClientLogic();
            litigationLogic = new LitigationLogic();
            judicialLitigationLogic = new JudicialLitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
        }

        public ActionResult Index(int? litigationId, int? judicialLitigation, int? adminLitigation, int? clientId)
        {
            return RedirectToAction("Create", "Observation", new { litigationId = litigationId, judicialLitigation = judicialLitigation, adminLitigation = adminLitigation, clientId = clientId });
        }

        [Auth(Operation = "OBSERVATION.ADD", ClientIdRequired = true)]
        public ActionResult Create(int? litigationId, int? judicialLitigation, int? adminLitigation, int? clientId)
        {
            if (clientId != null) { 
                ViewData["clientId"] = clientId; ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId); 
            }
            if (litigationId != null) { 
                ViewData["litigationId"] = litigationId; ViewData["recordNumber"] = litigationLogic.findLitigationById(litigationId).recordNumber; 
            }
            if (judicialLitigation != null) { 
                ViewData["judicialLitigationID"] = judicialLitigation; 
            }
            else { 
                ViewData["adminLitigationID"] = adminLitigation; 
            }
            return View();
        }

        [HttpPost]
        public ActionResult registerNewObservation(string Subject, DateTime? Date, string Body, int? litigationId, int? judicialLitigationId, int? adminLitigationId, int? clientId) 
        {
            try{
                if (!String.IsNullOrEmpty(Date.ToString())){
                    return getObservationWithDateNotNull(Subject, Date, Body, litigationId, judicialLitigationId, adminLitigationId, clientId);
                }
                else{
                    return getObservationDateNull(Subject, Body, litigationId, judicialLitigationId, adminLitigationId, clientId);
                }
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "OBSERVATION.VIEW", ClientIdRequired = true)]
        public ActionResult getObservationData(int? observationId, int? judicialLitigationId, int? adminLitigationId, int? clientId, int? litigationId) 
        {
            try{
                observation _Observation = getObservationRegistered(observationId, judicialLitigationId, adminLitigationId);
                if (_Observation == null)
                    return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
                if (clientId != null) { 
                    ViewData["clientId"] = clientId; 
                    ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId); 
                }
                if (litigationId != null) { 
                    ViewData["litigationId"] = litigationId; 
                    ViewData["recordNumber"] = litigationLogic.findLitigationById(litigationId).recordNumber; 
                }
                return View("ObservationRegistered");
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "OBSERVATION.EDIT", ClientIdRequired = true)]
        public ActionResult Edit(int? observationId, int? judicialLitigationId, int? adminLitigationId, int? clientId, int? litigationId)
        {
            try{
                observation _Observation = observationLogic.getObservationById(observationId);
                if (clientId != null)
                    ViewData["clientId"] = clientId; ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId); 
                if (litigationId != null)
                    ViewData["litigationId"] = litigationId; ViewData["recordNumber"] = litigationLogic.findLitigationById(litigationId).recordNumber; 
                if (judicialLitigationId != null && judicialLitigationId != 0)
                    ViewData["judicialLitigationId"] = judicialLitigationId; 
                else 
                    ViewData["adminLitigationId"] = adminLitigationId; 
                return View(_Observation);
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "OBSERVATION.REPORT", ClientIdRequired = true)]
        public ActionResult judicialObservationReport(int? judicialLitigationId, int? clientId)
        {
            try{
                if(judicialLitigationId.HasValue){
                    ViewData["judicialLitigation"] = judicialLitigationLogic.getJudicialLitigationByJudicialLitigationId(judicialLitigationId, litigationLogic);
                    ViewData["observations"] = observationLogic.getListOfObservationsFromJudicialLitigation(judicialLitigationId);
                    ViewData["client"] = clientLogic.findClientByClientId(clientId);
                    return View();
                }
                return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = clientId });
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [Auth(Operation = "OBSERVATION.REPORT", ClientIdRequired = true)]
        public ActionResult adminObservationReport(int? adminLitigationId, int? clientId)
        {
            try{
                if (adminLitigationId.HasValue){
                    ViewData["adminLitigation"] = adminLitigationLogic.getAdminLitigationByAdminLitigationId(adminLitigationId, litigationLogic);
                    ViewData["observations"] = observationLogic.getListOfObservationsFromAdminLitigation(adminLitigationId);
                    ViewData["client"] = clientLogic.findClientByClientId(clientId);
                    return View();
                }
                return RedirectToAction("Index", "Litigation", new { isTheMainPage = true, clientId = clientId });
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        public ActionResult updateObservation(int? observationId, int? judicialLitigationId, int? adminLitigationId, int? litigationId, int? clientId, string subject, DateTime? date, string body)
        {
            observation observationUpdated = observationLogic.findObservationById(observationId);
            observationUpdated.Subject = subject;
            observationUpdated.Date = date;
            observationUpdated.Body = body;
            observationLogic.updateObservation(observationUpdated);
            if (clientId != null) { 
                ViewData["clientId"] = clientId; 
                ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId); 
            }
            if (litigationId != null) { 
                ViewData["litigationId"] = litigationId; 
                ViewData["recordNumber"] = litigationLogic.findLitigationById(litigationId).recordNumber; 
            }
            return RedirectToAction("getObservationData", "Observation", new { observationId = observationId, judicialLitigationId = judicialLitigationId, adminLitigationId = adminLitigationId, clientId = clientId, litigationId = litigationId });
        }

        private observation getObservationRegistered(int? observationId, int? judicialLitigationId, int? adminLitigationId)
        {
            if (judicialLitigationId != null && judicialLitigationId != 0){
                ViewData["judicialLitigationId"] = judicialLitigationId;
                ViewData["listObservations"] = observationLogic.getListOfObservationsFromJudicialLitigation(judicialLitigationId);
            }
            else{
                ViewData["adminLitigationId"] = adminLitigationId;
                ViewData["listObservations"] = observationLogic.getListOfObservationsFromAdminLitigation(adminLitigationId);
            }
            observation _Observation = observationLogic.getObservationById(observationId);
            getValuesOfObservationSelected(_Observation);
            return _Observation;
        }

        private void getValuesOfObservationSelected(observation _Observation)
        {
            ViewData["id"] = _Observation.id;
            ViewData["subject"] = _Observation.Subject;
            ViewData["date"] = _Observation.Date;
            ViewData["body"] = _Observation.Body;
        }

        private int getDataForObservationRegistered(string subject, DateTime? date, string body, int? litigationId, int? judicialLitigationId, int? adminLitigationId, int? clientId)
        {
            ViewData["subject"] = subject;
            ViewData["date"] = date;
            ViewData["body"] = body;
            if (clientId != null) { 
                ViewData["clientId"] = clientId; ViewData["clientName"] = clientLogic.getClientNameByClientId(clientId); 
            }
            if (litigationId != null) { 
                ViewData["litigationId"] = litigationId; ViewData["recordNumber"] = litigationLogic.findLitigationById(litigationId).recordNumber; 
            
            }
            int lastObservationRegistered = getLastObservationIdRegistered(subject, date, body, litigationId, judicialLitigationId, adminLitigationId);
            ViewData["id"] = lastObservationRegistered;
            return lastObservationRegistered;
        }

        private int getLastObservationIdRegistered(string subject, 
                                                   DateTime? observationDate, 
                                                   string body, 
                                                   int? litigationId, 
                                                   int? judicialLitigationId, 
                                                   int? adminLitigationId) {
            DateTime? date = DateTime.Now;
            if(!String.IsNullOrEmpty(observationDate.ToString())){
                date = observationDate;
            }
            
            int lastObservationRegistered = observationLogic.registerNewObservation(subject, date, body, litigationId);
            
            if (judicialLitigationId != 0){
                ViewData["judicialLitigationId"] = judicialLitigationId;
                ViewData["listObservations"] = observationLogic.getListOfObservationsFromJudicialLitigation(judicialLitigationId);
            }
            else{
                ViewData["adminLitigationId"] = adminLitigationId;
                ViewData["listObservations"] = observationLogic.getListOfObservationsFromAdminLitigation(adminLitigationId);
            }
            return lastObservationRegistered;
        }

        private ActionResult getObservationWithDateNotNull(string subject, 
                                                           DateTime? date, 
                                                           string body, 
                                                           int? litigationId,
                                                           int? judicialLitigationId, 
                                                           int? adminLitigationId, 
                                                           int? clientId) {
                                                               int lastObservationRegistered = getDataForObservationRegistered(subject, date, body, litigationId, judicialLitigationId, adminLitigationId, clientId);
            if (lastObservationRegistered == 0)
                return RedirectToAction("Create", "Observation", new { pLitigation = litigationId, pJudicialLitigation = judicialLitigationId });
            return View("ObservationRegistered");
        }

        private ActionResult getObservationDateNull(string subject, 
                                                    string body, 
                                                    int? litigationId,
                                                    int? judicialLitigationId, 
                                                    int? adminLitigationId, 
                                                    int? clientId) {
            int lastObservationRegistered = getDataForObservationRegistered(subject, DateTime.Now, body, litigationId, judicialLitigationId, adminLitigationId, clientId);
            if (lastObservationRegistered == 0)
                return RedirectToAction("Create", "Observation", new { pLitigation = litigationId, pJudicialLitigation = judicialLitigationId });
            return View("ObservationRegistered");
        }

    }
}
