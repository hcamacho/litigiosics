﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LitigationICS.BLL;
using LitigationICS.DAL;

namespace ICS.Litigios.WebApp.Controllers
{
    public class ReportController : Controller
    {
        private LitigationLogic litigationLogic;
        private JudicialLitigationLogic judicialLitigationLogic;
        private AdminLitigationLogic adminLitigationLogic;
        private ClientLogic clientLogic;

        public ReportController() 
        {
            litigationLogic = new LitigationLogic();
            judicialLitigationLogic = new JudicialLitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
            clientLogic = new ClientLogic();
        }

        public ActionResult Index()
        {
            try {
                List<admin_litigation> listAdminLitigations = adminLitigationLogic.getListOfAdminLitigations();
                List<juditial_litigation> listJudicialLitigations = judicialLitigationLogic.getListOfJudicialLitigations();

                if (listAdminLitigations != null)
                    ViewData["adminLitigations"] = listAdminLitigations;
                if (listJudicialLitigations != null)
                    ViewData["judicialLitigations"] = listJudicialLitigations;
                return View();
            }
            catch (Exception ex) {
                System.Diagnostics.Debug.WriteLine(ex);
                return new HttpStatusCodeResult((int)System.Net.HttpStatusCode.BadRequest);
            }
        }
	}
}