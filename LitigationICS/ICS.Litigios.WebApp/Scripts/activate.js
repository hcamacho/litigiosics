﻿$(document).ready(function ()
{
    gotoGeneralReport();
    disableEstimate();
    disableEstimateByClick();
    activateKeyEventsForFilters();
    pagination();
    activateDatePickers();
    showPopUpModals();
    hidePopUpModals();
});

function activateKeyEventsForFilters()
{
    getLitigationFilter();
    getReportJudicialLitigationFilter();
    getReportAdminLitigationFilter();
    getReportObservationFilter();
    getClientFilter();
}

function hidePopUpModals()
{
    hideDialogLitigation();
    hideDialogEliminateLitigation();
    hideDialogEliminateLitigation();
}


function showPopUpModals()
{
    showDialogLitigation();
    showDialogEliminateLitigation();
    showDialogConfirmDelete();
}

function activateDatePickers() {
    formatDatePicker();
    setDatePickerRegisterForm();
    setDatePickerEditForm();
}

function getLitigationFilter()
{
    $('#litigationFilter').keyup(function () {
        var searchableText = $(this).val().toUpperCase();
        $('.list-group .litigation-item').each(function () {
            var currentLiText = $(this).text().toUpperCase(),
                showCurrentLiText = currentLiText.indexOf(searchableText) !== -1;
            $(this).toggle(showCurrentLiText);
        });
    });
}

function getReportJudicialLitigationFilter()
{
    $('#reportJudicialLitigationFilter').keyup(function () {
        var searchableText = $(this).val().toUpperCase();

        $('.list-group .judicialLitigation-item').each(function () {
            var currentLiText = $(this).text().toUpperCase(),
                showCurrentLiText = currentLiText.indexOf(searchableText) !== -1;
            $(this).toggle(showCurrentLiText);
        });
    });
}

function getReportAdminLitigationFilter()
{
    $('#reportAdminLitigationFilter').keyup(function () {
        var searchableText = $(this).val().toUpperCase();

        $('.list-group .adminLitigation-item').each(function () {
            var currentLiText = $(this).text().toUpperCase(),
                showCurrentLiText = currentLiText.indexOf(searchableText) !== -1;
            $(this).toggle(showCurrentLiText);
        });
    });
}

function getReportObservationFilter()
{
    $('#reportObservationFilter').keyup(function () {
        var searchableText = $(this).val().toUpperCase();

        $('.list-group .observation-item').each(function () {
            var currentLiText = $(this).text().toUpperCase(),
                showCurrentLiText = currentLiText.indexOf(searchableText) !== -1;
            $(this).toggle(showCurrentLiText);
        });
    });
}

function getClientFilter()
{
    $('#clientFilter').keyup(function () {
        var searchableText = $(this).val().toUpperCase();
        $('.list-group .client-item').each(function () {
            var currentLiText = $(this).text().toUpperCase(),
                showCurrentLiText = currentLiText.indexOf(searchableText) !== -1;
            $(this).toggle(showCurrentLiText);
        });
    });
}

function hideDialogLitigation()
{
    $("#btn-hide-modal").click(function (e) {
        e.preventDefault();
        $("#dialog-litigation").modal('hide');
    });
}

function hideDialogEliminateLitigation()
{
    $("#btn-hide-delete-modal").click(function (e) {
        e.preventDefault();
        $("#dialog-eliminate-litigation").modal('hide');
    });
}

function hideDialogConfirmDelete()
{
    $("#btn-hide-confirm-modal").click(function (e) {
        e.preventDefault();
        $("#dialog-confirm-delete").modal('hide');
    });
}

function showDialogLitigation()
{
    $(".btn-show-modal").click(function (e) {
        e.preventDefault();
        $("#dialog-litigation").modal('show');
    });
}

function showDialogEliminateLitigation()
{
    $(".btn-show-delete-modal").click(function (e) {
        e.preventDefault();
        $("#dialog-eliminate-litigation").modal('show');
    });
}

function showDialogConfirmDelete()
{
    $(".btn-confirm-delete").click(function (e) {
        e.preventDefault();
        $("#dialog-confirm-delete").modal('show');
    });
}

function pagination()
{
    var req_num_row = 10;
    var $tr = $('.table-pager tbody tr');
    var total_num_row = $tr.length;
    var num_pages = 0;

    if (total_num_row % req_num_row == 0) {
        num_pages = total_num_row / req_num_row;
    }

    if (total_num_row % req_num_row >= 1) {
        num_pages = total_num_row / req_num_row;
        num_pages++;
        num_pages = Math.floor(num_pages++);
    }

    for (var i = 1; i <= num_pages; i++) {
        $('#pagination').append("<a id='" + i + "' href='" + i + "'>" + i + "</a> ");
    }

    $("#1").addClass("selected-page");

    $tr.each(function (i) {
        $(this).hide();
        if (i + 1 <= req_num_row) {
            $tr.eq(i).show();
        }
    });

    $('#pagination a').click(function (e) {
        e.preventDefault();
        $tr.hide();
        var page = $(this).text();
        var temp = page - 1;
        var start = temp * req_num_row;
        $('#pagination .selected-page').removeClass('selected-page');
        $(this).addClass('selected-page');
        for (var i = 0; i < req_num_row; i++) {
            $tr.eq(start + i).show();
        }
    });
}

function gotoGeneralReport()
{
    $('#general-report').attr('href', '/Report/Index');
}

function getCancelarObservationOption()
{
    window.history.back(1);
}

function createNewClient()
{
    window.location.href = "/Client/Create";
}

function setJuditialValuesForDelete(litigationId, juditialLitigationId, recordNumber)
{
    $("#recordNumber").html(recordNumber);
    $("#litigationId").val(litigationId);
    $("#juditialLitigationId").val(juditialLitigationId);
}

function setAdminValuesForDelete(litigationId, adminLitigationId, recordNumber)
{
    $("#recordNumber").html(recordNumber);
    $("#litigationId").val(litigationId);
    $("#adminLitigationId").val(adminLitigationId);
}

function formatDatePicker()
{
    Date.prototype.yyyymmdd = function () {
        var yyyy = this.getFullYear().toString();
        var mm = (this.getMonth() + 1).toString();
        var dd = this.getDate().toString();
        return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
    };
}

function setDatePickerRegisterForm()
{
    currentDate = new Date();
    $(".datepicker-register-form").datepicker({
        locale: 'es',
        defaultDate: currentDate.yyyymmdd(),
        format: 'yyyy-mm-dd',
        autoclose: true
    });
}

function setDatePickerEditForm()
{
    $(".datepicker-edit-form").datepicker({
        locale: 'es',
        defaultDate: currentDate.yyyymmdd(),
        format: 'yyyy-mm-dd',
        autoclose: true
    });
}

function disableEstimate()
{
    if ($('#unrated').prop('checked')) {
        $('#estimate').attr('disabled', 'disabled');
        $('#estimate').val('');
    }
}

function disableEstimateByClick()
{
    if($('#unrated').prop('checked')){
        $('#estimate').attr('disabled', 'disabled');
        $('#estimate').val('');
    }

    $('#unrated').click(function (e){
        if ($('#unrated').prop('checked')) {
            $('#estimate').attr('disabled', 'disabled');
            $('#estimate').val('');
        }
        else {
            $('#estimate').removeAttr('disabled');
        }
    });
}