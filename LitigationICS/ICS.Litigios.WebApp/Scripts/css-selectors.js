﻿function clientActiveSelector()
{
    if(document.getElementsByClassName('getClientsId')[0]){
        var clientId = document.getElementsByClassName('getClientsId')[0].id;
        if (clientId != null) {
            $('#' + clientId + '.client-item').addClass("active");
        }
    }
}

function litigationActiveSelector()
{
    if (document.getElementsByClassName('getLitigationsId')[0]) {
        var litigationId = document.getElementsByClassName('getLitigationsId')[0].id;
        if (litigationId != null) {
            $('#' + litigationId + '.litigation-item').addClass("active");
        }
    }
}

function observationActiveSelector()
{
    if (document.getElementsByClassName('getObservationId')[0]){
        var observationId = document.getElementsByClassName('getObservationId')[0].id;
        if (observationId != null) {
            document.getElementById(observationId).className += " active";
        }
    }
}
