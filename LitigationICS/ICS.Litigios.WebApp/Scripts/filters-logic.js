﻿$(document).ready(function ()
{
    $("#filterLitigation").keyup(function ()
    {
        var filterText = $(this).val().toUpperCase();

        $(".list-group .litigationList").each(function ()
        {
            var currentListText = $(this).text();
            var showCurrentListText = currentListText.indexOf(filterText) !== -1;

            $(this).toggle(showCurrentListText);

        });
    });

    $("#filterClient").keyup(function ()
    {
        var filterText = $(this).val().toUpperCase();

        $(".list-group .clientList").each(function () {
            var currentListText = $(this).text();
            var showCurrentListText = currentListText.indexOf(filterText) !== -1;

            $(this).toggle(showCurrentListText);

        });
    });

    $("#filterJuditialLitigationsTable").keyup(function ()
    {
        var rex = new RegExp($(this).val(), 'i');
        $('.searchable tr').hide();
        $('.searchable tr').filter(function ()
        {
            return rex.test($(this).text());
        }).show();
    });

    $("#filterAdminLitigationsTable").keyup(function ()
    {
        var rex = new RegExp($(this).val(), 'i');
        $('.searchable tr').hide();
        $('.searchable tr').filter(function ()
        {
            return rex.test($(this).text());
        }).show();
    });
});