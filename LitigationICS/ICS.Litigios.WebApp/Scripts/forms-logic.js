﻿function getCancelarObservationOption()
{
    window.history.back(1);
}

function createNewClient()
{
    window.location.href = "/Client/Create";
}

function showObservation(observationId, juditialLitigationId, adminLitigationId, clientId, litigationId)
{
    var form = document.createElement("form");
    var inputJuditialLitigationId = document.createElement("input");
    var inputAdminLitigationId = document.createElement("input");
    var inputLitigationId = document.createElement("input");
    var inputObservationId = document.createElement("input");
    var inputClientId = document.createElement("input");


    form.method = "POST";
    form.action = "/Observation/getObservationData";

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    inputLitigationId.value = litigationId;
    inputLitigationId.name = "litigationId";
    form.appendChild(inputLitigationId);


    if (juditialLitigationId != 0)
    {
        inputJuditialLitigationId.value = juditialLitigationId;
        inputJuditialLitigationId.name = "juditialLitigationId";
        form.appendChild(inputJuditialLitigationId);

        inputObservationId.value = observationId;
        inputObservationId.name = "observationId";
        form.appendChild(inputObservationId);
    }
    else
    {
        inputAdminLitigationId.value = adminLitigationId;
        inputAdminLitigationId.name = "adminLitigationId";
        form.appendChild(inputAdminLitigationId);

        inputObservationId.value = observationId;
        inputObservationId.name = "observationId";
        form.appendChild(inputObservationId);
    }

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToShowClientInformation(clientId)
{
    var form = document.createElement("form");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Client/showClientInformation";

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function showLitigation(type, litigationId, clientId)
{
    var form = document.createElement("form");
    var inputType = document.createElement("input");
    var inputLitigationId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Litigation/selectJuditialOrAdminLitigation";

    inputType.value = type;
    inputType.name = "type";
    form.appendChild(inputType);

    inputLitigationId.value = litigationId;
    inputLitigationId.name = "litigationId";
    form.appendChild(inputLitigationId);

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToLitigationsPage()
{
    var form = document.createElement("form");
    var inputMainPage = document.createElement("input");
    
    form.method = "POST";
    form.action = "/Litigation/Index";

    inputMainPage.value = "true";
    inputMainPage.name = "isTheMainPage";
    form.appendChild(inputMainPage);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToLitigationsPageByClientId(clientId)
{

    var form = document.createElement("form");
    var inputMainPage = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Litigation/Index";

    inputMainPage.value = "false";
    inputMainPage.name = "isTheMainPage";
    form.appendChild(inputMainPage);

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToJuditialLitigationsPageByClientId(juditialLitigationId, clientId)
{
    var form = document.createElement("form");
    var inputJuditialId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/JuditialLitigation/Index";

    inputJuditialId.value = juditialLitigationId;
    inputJuditialId.name = "pJuditialLitigationID";
    form.appendChild(inputJuditialId);

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToLitigationSelected(litigationId, clientId) {
    var form = document.createElement("form");
    var inputLitigationId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Litigation/backToLitigation";

    inputLitigationId.value = litigationId;
    inputLitigationId.name = "litigationId";
    form.appendChild(inputLitigationId);

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToAdminLitigationsPageByClientId(adminLitigationId, clientId)
{
    var form = document.createElement("form");
    var inputJuditialId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/AdminLitigation/detailAdminLitigation";

    inputJuditialId.value = adminLitigationId;
    inputJuditialId.name = "pAdminLitigationID";
    form.appendChild(inputJuditialId);

    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);

    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function isNumber(evt)
{
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode

    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;
    return true;
}