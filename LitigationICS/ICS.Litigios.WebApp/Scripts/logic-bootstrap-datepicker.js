﻿$(document).ready(function ()
{
    $('.datepicker').datepicker({
        dateFormat: 'yy-mm-dd',
        setStartDate: '2016-06-15',
        todayHighlight: true,
        autoclose: true
    });
});

