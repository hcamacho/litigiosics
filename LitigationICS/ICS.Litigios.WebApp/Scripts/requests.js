﻿function showObservation(observationId, juditialLitigationId, adminLitigationId, clientId, litigationId)
{
    window.history.back(1);
}

function createNewClient()
{
    window.location.href = "/Client/Create";
}

function setJudicialValuesForDelete(litigationId, judicialLitigationId, recordNumber)
{
    $("#recordNumber").html(recordNumber);
    $("#litigationId").val(litigationId);
    $("#judicialLitigationId").val(judicialLitigationId);
}

function setAdminValuesForDelete(litigationId, adminLitigationId, recordNumber) {
    $("#recordNumber").html(recordNumber);
    $("#litigationId").val(litigationId);
    $("#adminLitigationId").val(adminLitigationId);
}

function goToGeneralReport()
{
    var form = document.createElement("form");
    form.method = "POST";
    form.action = "/Report/Index";
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function showObservation(observationId, judicialLitigationId, adminLitigationId, clientId, litigationId)
{
    var form = document.createElement("form");
    var inputJudicialLitigationId = document.createElement("input");
    var inputAdminLitigationId = document.createElement("input");
    var inputLitigationId = document.createElement("input");
    var inputObservationId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Observation/getObservationData";
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    inputLitigationId.value = litigationId;
    inputLitigationId.name = "litigationId";
    form.appendChild(inputLitigationId);

    if (judicialLitigationId != 0) {
        inputJudicialLitigationId.value = judicialLitigationId;
        inputJudicialLitigationId.name = "judicialLitigationId";
        form.appendChild(inputJudicialLitigationId);
    }
    else{
        inputAdminLitigationId.value = adminLitigationId;
        inputAdminLitigationId.name = "adminLitigationId";
        form.appendChild(inputAdminLitigationId);
    }

    inputObservationId.value = observationId;
    inputObservationId.name = "observationId";
    form.appendChild(inputObservationId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToShowClientInformation(clientId)
{
    var form = document.createElement("form");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Client/showClientInformation";
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToShowContacts(clientId) {
    var form = document.createElement("form");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Contact/Index";
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToClientsDetail(clientId) {
    var form = document.createElement("form");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Client/detailClients";
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToContactsDetail(clientId) {
    var form = document.createElement("form");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Contact/showContacts";
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function gotoJudicialObservationReport(judicialLitigationId, clientId) {
    var form = document.createElement("form");
    var inputLitigationId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Observation/judicialObservationReport";
    inputLitigationId.value = judicialLitigationId;
    inputLitigationId.name = "judicialLitigationId";
    form.appendChild(inputLitigationId);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function gotoAdminObservationReport(adminLitigationId, clientId) {
    var form = document.createElement("form");
    var inputLitigationId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Observation/adminObservationReport";
    inputLitigationId.value = adminLitigationId;
    inputLitigationId.name = "adminLitigationId";
    form.appendChild(inputLitigationId);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function showLitigation(type, litigationId, clientId)
{
    var form = document.createElement("form");
    var inputType = document.createElement("input");
    var inputLitigationId = document.createElement("input");
    var inputClientId = document.createElement("input");

    form.method = "POST";
    form.action = "/Litigation/selectJudicialOrAdminLitigation";
    inputType.value = type;
    inputType.name = "type";
    form.appendChild(inputType);
    inputLitigationId.value = litigationId;
    inputLitigationId.name = "litigationId";
    form.appendChild(inputLitigationId);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function showContactById(clientId, contactId)
{
    var form = document.createElement("form");
    var inputClientId = document.createElement("input");
    var inputContactId = document.createElement("input");

    form.method = "POST";
    form.action = "/Contact/showContact";
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    inputContactId.value = contactId;
    inputContactId.name = "contactId";
    form.appendChild(inputContactId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToLitigationsPage(clientId)
{
    var form = document.createElement("form");
    var inputMainPage = document.createElement("input");
    var inputClientId = document.createElement("input");
    form.method = "POST";
    form.action = "/Litigation/Index";
    inputMainPage.value = "true";
    inputMainPage.name = "isTheMainPage";
    form.appendChild(inputMainPage);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToLitigationsPageByClientId(clientId)
{

    var form = document.createElement("form");
    var inputMainPage = document.createElement("input");
    var inputClientId = document.createElement("input");
    form.method = "POST";
    form.action = "/Litigation/Index";
    inputMainPage.value = "false";
    inputMainPage.name = "isTheMainPage";
    form.appendChild(inputMainPage);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToJudicialLitigationsPageByClientId(judicialLitigationId, clientId)
{
    var form = document.createElement("form");
    var inputJudicialId = document.createElement("input");
    var inputClientId = document.createElement("input");
    form.method = "POST";
    form.action = "/JudicialLitigation/Index";
    inputJudicialId.value = judicialLitigationId;
    inputJudicialId.name = "judicialLitigationId";
    form.appendChild(inputJudicialId);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToLitigationSelected(litigationId, clientId) {
    var form = document.createElement("form");
    var inputLitigationId = document.createElement("input");
    var inputClientId = document.createElement("input");
    form.method = "POST";
    form.action = "/Litigation/backToLitigation";
    inputLitigationId.value = litigationId;
    inputLitigationId.name = "litigationId";
    form.appendChild(inputLitigationId);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function goToAdminLitigationsPageByClientId(adminLitigationId, clientId)
{
    var form = document.createElement("form");
    var inputJudicialId = document.createElement("input");
    var inputClientId = document.createElement("input");
    form.method = "POST";
    form.action = "/AdminLitigation/detailAdminLitigation";
    inputJudicialId.value = adminLitigationId;
    inputJudicialId.name = "adminLitigationId";
    form.appendChild(inputJudicialId);
    inputClientId.value = clientId;
    inputClientId.name = "clientId";
    form.appendChild(inputClientId);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function isNumber(evt)
{
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;
    return true;
}