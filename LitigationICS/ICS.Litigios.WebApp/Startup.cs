﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ICS.Litigios.WebApp.Startup))]
namespace ICS.Litigios.WebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
