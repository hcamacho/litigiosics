﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LitigationICS.DAL;

namespace LitigationICS.BLL
{
    public class AdminLitigationLogic
    {
        public AdminLitigationLogic()
        {
        }

        public admin_litigation getAdminLitigationByLitigationId(int? litigationId)
        {
            admin_litigation adminLitigation = IConstants.db.admin_litigation
                .Where(k => k.idLitigation == litigationId)
                .Select(al => new
                {
                    id = al.id
                }).ToList()
                .Select(al => new admin_litigation
                {
                    id = al.id
                }).FirstOrDefault();

            return adminLitigation;
        }

        public admin_litigation findAdminLitigationByLitigationId(int? litigationId)
        {
            admin_litigation aLitigation = IConstants.db.admin_litigation
                .Where(k => k.idLitigation == litigationId)
                .Select(adminLitigation => adminLitigation).FirstOrDefault();
            return aLitigation;
        }

        public int? getAdminLitigationIdByLitigationId(int? litigationId)
        {
            int? adminLitigationId = IConstants.db.admin_litigation
                .Where(k => k.idLitigation == litigationId)
                .Select(adminLitigation => adminLitigation).FirstOrDefault().id;
            return adminLitigationId;
        }

        public List<admin_litigation> getListOfAdminLitigations()
        {
            List<admin_litigation> adminLitigations = getListOfAdminLitigation();
            return adminLitigations;
        }

        public List<admin_litigation> getListOfAdminLitigationsByClientId(int? clientId)
        {
            List<admin_litigation> adminLitigations = getAdminLitigationByClientId(clientId);
            return adminLitigations;
        }

        public admin_litigation getAdminLitigationByAdminLitigationId(int? adminLitigation, LitigationLogic litigationLogic)
        {
            admin_litigation adminLitigationSelected = getAdminLitigationById(adminLitigation, litigationLogic);
            return adminLitigationSelected;
        }

        public SelectList getAdminLitigationState(string stateName)
        {
            return new SelectList(loadAdminStatesTheirOwnDropDown(), IConstants.State, IConstants.State, stateName);
        }

        public bool getDeleteAdminLitigation(int? adminLitigationId, LitigationLogic litigationLogic)
        {
            try{
                deleteAdminLitigation(adminLitigationId, litigationLogic);
                return true;
            }
            catch{
                return false;
            }
        }

        private admin_litigation getAdminLitigationById(int? adminLitigation, LitigationLogic litigationLogic){
            var al = IConstants.db.admin_litigation.Where(myAdminLitigation =>
                        myAdminLitigation.id == adminLitigation).FirstOrDefault();
            admin_litigation adminLitigationSelected = new admin_litigation{
                id = al.id,
                litigation = litigationLogic.findLitigationById(al.idLitigation),
                transferCharges = al.transferCharges,
                Type = al.Type,
                TaxAdmin = al.TaxAdmin,
            };
            return adminLitigationSelected;
        }

        private void deleteAdminLitigation(int? adminLitigationId, LitigationLogic litigationLogic)
        {
            admin_litigation adminLitigation = getAdminLitigationByAdminLitigationId(adminLitigationId, litigationLogic);
            IConstants.db.Entry(adminLitigation).State = System.Data.Entity.EntityState.Deleted;
            IConstants.db.SaveChanges();
        }

        private List<state> loadAdminStatesTheirOwnDropDown()
        {
            List<state> state = new List<state>();

            state firstItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.firstAdminState))
                .Select(s => s).FirstOrDefault();
            state secondItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.secondAdminState))
                .Select(s => s).FirstOrDefault();
            state thirdItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.thirdAdminState))
                .Select(s => s).FirstOrDefault();
            state fourthItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.fourthAdminState))
                .Select(s => s).FirstOrDefault();
            state fifthItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.fifthAdminState))
                .Select(s => s).FirstOrDefault();
            state sixthItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.sixthAdminState))
                .Select(s => s).FirstOrDefault();
            state seventhItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.seventhAdminState))
                .Select(s => s).FirstOrDefault();

            state.Add(firstItemState);
            state.Add(secondItemState);
            state.Add(thirdItemState);
            state.Add(fourthItemState);
            state.Add(fifthItemState);
            state.Add(sixthItemState);
            state.Add(seventhItemState);
            return state;
        }

        private List<admin_litigation> getListOfAdminLitigation()
        {
            List<admin_litigation> adminLitigations = new List<admin_litigation>();
            IConstants.db.client_litigation
                .ToList()
                .ForEach(cl => {
                    cl.litigation.admin_litigation.ToList().ForEach(al =>{
                        adminLitigations.Add(new admin_litigation{
                            id = al.id,
                            TaxAdmin = al.TaxAdmin,
                            transferCharges = al.transferCharges,
                            Type = al.Type,
                            litigation = al.litigation,
                            idLitigation = al.idLitigation
                        });
                    });
                });
            return adminLitigations;
        }

        private List<admin_litigation> getAdminLitigationByClientId(int? clientId)
        {
            List<admin_litigation> adminLitigations = new List<admin_litigation>();
            IConstants.db.client_litigation
                .Where(cl => cl.idClient == clientId)
                .ToList()
                .ForEach(cl => {
                    cl.litigation.admin_litigation.ToList().ForEach(al => {
                        adminLitigations.Add(new admin_litigation {
                            id = al.id,
                            TaxAdmin = al.TaxAdmin,
                            transferCharges = al.transferCharges,
                            Type = al.Type,
                            litigation = al.litigation,
                            idLitigation = al.idLitigation
                        });
                    });
                });
            return adminLitigations;
        }
    }
}
