﻿using ICS.Litigios.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LitigationICS.BLL
{
    public static class AuthOperationLogic
    {
        #region DATA must be replaced with DAL
        private static List<AuthOperation> operations = new List<AuthOperation>();

        private static void addOp(string pName, bool pRestricted, List<string> pRoleNames)
        {
            operations.Add(new AuthOperation
            {
                id = operations.Count,
                name = pName.ToUpper(),
                isRestricted = pRestricted,
                roles = AuthRoleLogic.getRoles().Where(r => pRoleNames.Contains(r.name)).ToList()
            });
        }

        static AuthOperationLogic()
        {
            addOp("LITIGATION.VIEW", true, new List<string> { "administrator", "client" });
            addOp("LITIGATION.ADD", true, new List<string> { "administrator" });
            addOp("LITIGATION.EDIT", true, new List<string> { "administrator" });
            addOp("LITIGATION.DELETE", true, new List<string> { "administrator" });
            addOp("CONTACT.VIEW", true, new List<string> { "administrator", "client" });
            addOp("CONTACT.ADD", true, new List<string> { "administrator", "client" });
            addOp("CLIENT.VIEW", true, new List<string> { "administrator", "client" });
            addOp("CLIENT.REPORT", true, new List<string> { "administrator" });
            addOp("CLIENT.ADD", true, new List<string> { "administrator" });
            addOp("CLIENT.EDIT", true, new List<string> { "administrator"});
            addOp("OBSERVATION.VIEW", true, new List<string> { "administrator", "client" });
            addOp("OBSERVATION.REPORT", true, new List<string> { "administrator", "client" });
            addOp("OBSERVATION.ADD", true, new List<string> { "administrator" });
            addOp("OBSERVATION.EDIT", true, new List<string> { "administrator" });

        }
        #endregion

        public static AuthOperation getOperationByName(string pOperationName)
        {
            return operations.FirstOrDefault(o => o.name.Equals(pOperationName.ToUpper()));
        }

        public static List<AuthOperation> getOperations()
        {
            return operations;
        }
    }
}
