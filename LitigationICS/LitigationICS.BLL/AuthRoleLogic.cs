﻿using ICS.Litigios.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LitigationICS.BLL
{
    public static class AuthRoleLogic
    {
        #region DATA must be replaced with DAL
        private static List<AuthRole> roles = new List<AuthRole>{
            new AuthRole{
                id = 1,
                name = IConstants.administratorRole,
                isRestricted = false
            },
            new AuthRole{
                id = 2,
                name = IConstants.clientRole,
                isRestricted = true
            }
        };
        #endregion

        public static List<AuthRole> getRoles()
        {
            return roles;
        }
    }
}
