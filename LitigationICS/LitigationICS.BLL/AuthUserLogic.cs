﻿using ICS.Litigios.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace LitigationICS.BLL
{
    public static class AuthUserLogic
    {
        #region DATA must be replaced with DAL
        private static List<AuthUser> users = new List<AuthUser>(){ 
            new AuthUser{
                    id = 1,
                    userName = "admin",
                    password ="admin",
                    name = "Administrador",
                    email = "hmuir@pernix.cr",
                    clients = new List<string>(),
                    roles = new List<AuthRole>(){ AuthRoleLogic.getRoles().First(r => r.name.Equals(IConstants.administratorRole)) }
                }
        };
        #endregion

        public static AuthUser getUserByName(String userName)
        {
            //TODO: LOAD USERS FROM THE DATABASE
            return users.FirstOrDefault(u => u.userName.Equals(userName));
        }

        public static AuthUser authenticateUser(String userName, String userPassword)
        {
            AuthUser user = getUserByName(userName);
            if (user == null)
            {
                return null;
            }
            //TODO: PASSWORD MUST BE CYPHER
            if (!user.password.Equals(userPassword))
            {
                return null;
            }
            return user;
        }

        public static bool isAuthorized(String userName, String operationName, String client)
        {
            bool authorized = false;
            AuthUser user = getUserByName(userName);
            AuthOperation operation = AuthOperationLogic.getOperationByName(operationName);
            if (operation == null)
            {
                authorized = true; //Si la operación no esta mapeada en la seguridad se da el permiso
            }
            else
            {
                if (user == null)
                {
                    throw new Exception(IConstants.failedUserAuth);
                }
                authorized = operation.roles.Exists(or => user.roles.Exists(ur => ur.name.Equals(or.name)))
                    && (!operation.isRestricted ||
                    user.clients.Contains(client) ||
                    operation.roles.Exists(or => user.roles.Exists(ur => ur.name.Equals(or.name) && !ur.isRestricted))
                    );
            }
            return authorized;
        }
    }
}
