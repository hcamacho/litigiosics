﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using LitigationICS.DAL;
using ICS.Litigios.Entities;
using System.Web.Mvc;

namespace LitigationICS.BLL
{
    public class ClientLogic
    {
        private PersonLogic personLogic;
        private ContactLogic contactLogic;
        private LegalRepresentativeLogic legalRepresentativeLogic;

        public ClientLogic()
        {
            personLogic = new PersonLogic();
            contactLogic = new ContactLogic();
            legalRepresentativeLogic = new LegalRepresentativeLogic();
        }

        public int getRegisterNewClient(int legalRepresentativeId, int? personId, int? contactId)
        {
            int lastClientRegistered = registerNewClient(legalRepresentativeId, personId, contactId);
            return lastClientRegistered;
        }

        public void updateClient(client clientUpdated)
        {
            IConstants.db.Entry(clientUpdated);
            IConstants.db.SaveChanges();
        }

        public void updateContact(person parameterPerson, contact parameterContact, user_email parameterEmail, user_phone parameterPhone)
        {
            person person = new person { 
                Name = parameterPerson.Name, 
                LastName = parameterPerson.LastName, 
                DNI = parameterPerson.DNI 
            };

            user_phone phoneUser = new user_phone { 
                Phone = parameterPhone.Phone, 
                person = person 
            };

            user_email emailUser = new user_email { 
                Email = parameterEmail.Email,
                person = person 
            };

            contact contact = new contact { 
                Position = parameterContact.Position, 
                person = person 
            };

            updateContact(contact, emailUser, phoneUser);
        }

        public List<client> getListOfClientsWithLitigation()
        {
            List<client> clients = getListOfClientsIdAndName();
            return clients;
        }

        public List<client> getListOfClientsWithLitigationByClientId(int? clientId)
        {
            List<client> clients = getListOfClientsIdAndNameByClientId(clientId);
            return clients;
        }

        public client getClientIdAndNameByClientId(int? clientId)
        {
            client _Client = IConstants.db.client
                .Where(c => c.id == clientId)
                .Select(c => new {
                    id = c.id,
                    person = c.person
                }).ToList()
                .Select(c => new client {
                    id = c.id,
                    person = c.person
                }).FirstOrDefault();
            return _Client;
        }

        public List<client> getListOfClients()
        {
            List<client> listClients = new List<client>();
            return getClientAndConcatSearchableText(listClients);
        }

        public bool exitSomeClient()
        {
            bool exit = false;

            try{
                client client = IConstants.db.client.Select(k => k).FirstOrDefault();
                if (client != null)
                    exit = true;
            }
            catch { 
                exit = false; 
            }
            return exit;
        }

        public string getClientNameByClientId(int? clientId)
        {
            string clientName = IConstants.db.client
                .Where(k => k.id == clientId)
                .Select(client => client).FirstOrDefault().person.Name;
            return clientName;
        }

        public int getLastClientIdRegistratedWithLitigation()
        {
            int lastClientWithLitigation = 0;
            try {
                lastClientWithLitigation = IConstants.db.client_litigation
                    .Select(cl => new {
                        id = cl.client.id
                    })
                    .ToList()
                    .Max(c => c.id);
            }
            catch {
                lastClientWithLitigation = 0;
            }
            return lastClientWithLitigation;
        }

        public SelectList getListOfPersonsNameForDropDown()
        {
            return new SelectList(IConstants.db.client, "id", "person.Name");
        }

        public client findClientByClientId(int? clientId)
        {
            client _Client = IConstants.db.client.Find(clientId);
            return _Client;
        }

        public person findPersonByPersonId(int? personId)
        {
            person _Person = IConstants.db.person.Find(personId);
            return _Person;
        }

        public contact findContactByContactId(int? contactId)
        {
            contact _Contact = IConstants.db.contact.Find(contactId);
            return _Contact;
        }

        public legalrepresentative findLegalRepresentativeByLegalRepresentativeId(int? legalRepresentativeId) 
        {
            legalrepresentative legalRepresentative = IConstants.db.legalrepresentative.Find(legalRepresentativeId);
            return legalRepresentative;
        }

        public user_phone findPhoneByPhoneId(int? phoneId)
        {
            user_phone _Phone = IConstants.db.user_phone
                .Where(k => k.id == phoneId)
                .Select(phone => phone).FirstOrDefault();
            return _Phone;
        }

        public user_email findEmailByEmailId(int? emailId)
        {
            user_email _Email = IConstants.db.user_email
                .Where(k => k.id == emailId)
                .Select(email => email).FirstOrDefault();
            return _Email;
        }

        private List<client> getClientAndConcatSearchableText(List<client> listClients)
        {
            IConstants.db.client.ToList().ForEach(clients => {
                string text = clients.person.Name;
                clients.client_litigation.ToList()
                    .ForEach(l => {
                        text += l.litigation.nextStepDate;
                        text += l.litigation.nextStepDetail;
                        l.litigation.admin_litigation.ToList()
                        .ForEach(a => {
                            text += a.TaxAdmin;
                            text += a.transferCharges;
                            text += a.Type;
                        });
                        l.litigation.juditial_litigation.ToList()
                        .ForEach(j => {
                            text += j.Actor;
                            text += j.Defendant;
                            text += j.Court;
                        });
                    });
                listClients.Add(
                    new client {
                        id = clients.id,
                        person = clients.person,
                        searchableText = text.ToUpper()
                    });
            });
            return listClients;
        }

        private int registerNewClient(int legalRepresentativeId, int? personId, int? contactId)
        {
            int lastClientRegistered = 0;
            client client = null;
            client = new client { idLegalRepresentative = legalRepresentativeId, idPerson = personId };
            IConstants.db.client.Add(client);
            lastClientRegistered = saveClient(client);

            if (lastClientRegistered > 0){
                return lastClientRegistered;
            }
            return 0;
        }

        private int saveClient(client clientInserted)
        {
            IConstants.db.SaveChanges();
            return clientInserted.id;
        }

        private void updateContact(contact contactUpdated, user_email emailUpdated, user_phone phoneUpdated)
        {
            IConstants.db.Entry(emailUpdated);
            IConstants.db.Entry(phoneUpdated);
            IConstants.db.Entry(contactUpdated);
            IConstants.db.SaveChanges();
        }

        private List<client> getListOfClientsIdAndName()
        {
            List<client> clients = IConstants.db.client_litigation
                .GroupBy(cl => new { 
                    cl.client.id,
                    cl.client.person.Name 
                })
                .Select(gr => new {
                    id = gr.Key.id,
                    name = gr.Key.Name
                }).ToList()
                .Select(c => new client {
                    id = c.id,
                    groupByName = c.name
                }).ToList();
            return clients;
        }

        private List<client> getListOfClientsIdAndNameByClientId(int? clientId)
        {
            List<client> clients = IConstants.db.client_litigation
                .Where(c => c.client.id == clientId)
                .GroupBy(clientLitigation => new { clientLitigation.client.id, clientLitigation.client.person.Name })
                .Select(grouping => new client() {
                    id = grouping.Key.id,
                    groupByName = grouping.Key.Name
                }).ToList();
            return clients;
        }
    }
}
