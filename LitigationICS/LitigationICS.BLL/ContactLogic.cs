﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitigationICS.DAL;

namespace LitigationICS.BLL
{
    public class ContactLogic
    {
        public ContactLogic() 
        {
        }

        public int registerNewContact(string position, int? personId)
        {
            int lastContactRegistered = registerContact(position, personId);
            return lastContactRegistered;
        }

        public void registerNewClientContact(int? contactId, int? clientId) 
        {
            registerClientContact(contactId, clientId);
        }

        public contact findContactByContactId(int? contactId)
        {
            contact _Contact = IConstants.db.contact.Find(contactId);
            return _Contact;
        }

        public client_contact getClientContactByClientId(int? clientId) 
        {
            client_contact client_contact = IConstants.db.client_contact.Where(cc => cc.clientId == clientId)
                                                             .Select(cc => cc).FirstOrDefault();
            return client_contact;
        }

        public List<contact> getListOfContactsByClientId(int? clientId) 
        {
            List<contact> listContacts = IConstants.db.client_contact.Where(cc => cc.clientId == clientId)
                                                          .Select(cc => cc.contact).ToList();
            return listContacts;
        }

        private int registerContact(string position, int? personId)
        {
            int lastContactRegistered = 0;
            contact contact = new contact{
                Position = position,
                idPerson = personId
            };
            IConstants.db.contact.Add(contact);
            lastContactRegistered = saveContact(contact);
            if (lastContactRegistered > 0){
                return lastContactRegistered;
            }
            return 0;
        }

        private void registerClientContact(int? contactId, int? clientId)
        {
            client_contact client_contact = new client_contact {
                clientId = clientId,
                contactId = contactId
            };
            IConstants.db.client_contact.Add(client_contact);
            IConstants.db.SaveChanges();    
        }

        private int saveContact(contact contactInserted)
        {
            IConstants.db.SaveChanges();
            return contactInserted.id;
        }

    }
}
