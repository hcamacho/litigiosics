﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using ICS.Litigios.Entities;

namespace LitigationICS.BLL
{
    public class EmailServerLogic
    {
        private MailMessage mailMessage;
        private SmtpClient smtpServer;

        public EmailServerLogic() 
        {
            mailMessage = new MailMessage();
            smtpServer = new SmtpClient();
        }

        public void sendMail(Email email)
        {
            mailMessage.To.Add(email.to);
            mailMessage.From = new MailAddress(IConstants.from);
            mailMessage.Subject = email.subject;
            LinkedResource linkedResource = new LinkedResource(email.logoPath);
            linkedResource.ContentId = IConstants.logo;
            mailMessage.IsBodyHtml = true;
            AlternateView alternativeView = AlternateView.CreateAlternateViewFromString(email.content);
            mailMessage.AlternateViews.Add(alternativeView);
            mailMessage.CC.Add(IConstants.from);
            smtpServer.Host = IConstants.host;
            smtpServer.Port = IConstants.port;
            smtpServer.UseDefaultCredentials = false;
        }

    }
}
