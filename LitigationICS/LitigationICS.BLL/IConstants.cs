﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitigationICS.DAL;

namespace LitigationICS.BLL
{
    public class IConstants
    {
        public static LitigiosICSEntities db = new LitigiosICSEntities();
        public static string dateFormat = "yyyy-MM-dd";
        public static string host = "pernix-solutions";
        public static int port = 587;
        public static string from = "info@icsabogados.com";
        public static string logo = "logo";
        public static string State = "State1";
        public static string Type = "Type1";
        public static string administratorRole = "administrator";
        public static string clientRole = "client";
        public static string failedRegisterContact = "No es posible registrar el contacto anterior, como observación debe de llenar los datos de <Correo> y <Teléfono>";
        public static string clientWithoutContact = "El cliente seleccionado no posee contactos disponibles!";
        public static string failedRegisterClient = "No es posible registrar el cliente, por alguna de las siguientes razones: ";
        public static string failedUserAuth = "Usuario no autenticado, contraseña incorrecta.";
        public static string firstJudicialState = "Admisibilidad de la Demanda";
        public static string secondJudicialState = "Réplica";
        public static string thirdJudicialState = "Conciliación";
        public static string fourthJudicialState = "Audiencia Preliminar";
        public static string fifthJudicialState = "Audiencia de Juicio";
        public static string sixthJudicialState = "Casación";
        public static string seventhJudicialState = "Ejecución";
        public static string eighthJudicialState = "Admisibilidad de Medida Cautelar";
        public static string ninethJudicialState = "Recurso de Apelación";
        public static string tenthJudicialState = "Audiencia Oral. Recurso de Apelación";
        public static string eleventhJudicialState = "N/A";
        public static string firstAdminState = "Inicio de Fiscalización";
        public static string secondAdminState = "Propuesta Provisional";
        public static string thirdAdminState = "Propuesta Definitiva";
        public static string fourthAdminState = "Impugnación";
        public static string fifthAdminState = "Recurso de Revocatoria";
        public static string sixthAdminState = "Recurso de Apelación";
        public static string seventhAdminState = "Resolución TFA";
        private static List<StringBuilder> listOfObservations;

        public static List<StringBuilder> getListOfObservations()
        {
            listOfObservations = new List<StringBuilder>();
            listOfObservations.Add(new StringBuilder("1. Repitió el nombre del cliente a registrar"));
            listOfObservations.Add(new StringBuilder("2. Dejo vacío el campo con el nombre del cliente"));
            listOfObservations.Add(new StringBuilder("3. Incumplió con alguna validación del sistema"));
            return listOfObservations;
        }
    }
}
