﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LitigationICS.DAL;

namespace LitigationICS.BLL
{
    public class JudicialLitigationLogic
    {
        public JudicialLitigationLogic()
        {
        }

        public juditial_litigation getJudicialLitigationByLitigationId(int? litigationId)
        {
            juditial_litigation judicialLitigation = IConstants.db.juditial_litigation
                .Where(k => k.idLitigation == litigationId)
                .Select(jl => new
                {
                    id = jl.id
                })
                .ToList()
                .Select(jl => new juditial_litigation
                {
                    id = jl.id
                }).FirstOrDefault();

            return judicialLitigation;
        }

        public juditial_litigation findJudicialLitigationByLitigationId(int? litigationId)
        {
            juditial_litigation jLitigation = IConstants.db.juditial_litigation
                .Where(k => k.idLitigation == litigationId)
                .Select(judicialLitigation => judicialLitigation)
                .FirstOrDefault();
            return jLitigation;
        }

        public int? getJudicialLitigationIdByLitigationId(int? litigationId)
        {
            int? judicialLitigationId = IConstants.db.juditial_litigation
                .Where(k => k.idLitigation == litigationId)
                .Select(judicialLitigation => judicialLitigation)
                .FirstOrDefault().id;
            return judicialLitigationId;
        }

        public List<juditial_litigation> getListOfJudicialLitigations()
        {
            List<juditial_litigation> judicialLitigations = getListOfJudicialLitigation();
            return judicialLitigations;
        }

        public List<juditial_litigation> getListOfJudicialLitigationsByClientId(int? clientId)
        {
            List<juditial_litigation> judicialLitigations = getJudicialLitigationByClientId(clientId);
            return judicialLitigations;
        }

        public juditial_litigation getJudicialLitigationByJudicialLitigationId(int? judicialLitigationId, LitigationLogic litigationLogic)
        {
            juditial_litigation judicialLitigationSelected = getJudicialLitigationByJudicialId(judicialLitigationId, litigationLogic);
            return judicialLitigationSelected;
        }

        public SelectList getJudicialLitigationState(string stateName)
        {
            return new SelectList(loadJudicialStatesTheirOwnDropDown(), IConstants.State, IConstants.State, stateName);
        }

        public SelectList getJudicialLitigationCautionState(string stateName)
        {
            return new SelectList(loadJudicialCautionStatesTheirOwnDropDown(), IConstants.State, IConstants.State, stateName);
        }

        public bool getDeleteJudicialLitigation(int? judicialLitigationId, LitigationLogic litigationLogic)
        {
            try{
                deleteJudicialLitigation(judicialLitigationId, litigationLogic);
                return true;
            }
            catch{
                return false;
            }
        }

        private juditial_litigation getJudicialLitigationByJudicialId(int? judicialLitigationId, LitigationLogic litigationLogic)
        {
            var jl = IConstants.db.juditial_litigation.Where(myJudicialLitigation =>
                        myJudicialLitigation.id == judicialLitigationId).FirstOrDefault();
            juditial_litigation judicialLitigationSelected = new juditial_litigation{
                id = jl.id,
                Court = jl.Court,
                Actor = jl.Actor,
                Defendant = jl.Defendant,
                Adjuvant = jl.Adjuvant,
                cautionActualState = jl.cautionActualState,
                cautionNextStepDate = jl.cautionNextStepDate,
                cautionNextStepDetail = jl.cautionNextStepDetail,
                litigation = litigationLogic.findLitigationById(jl.litigation.id)
            };
            return judicialLitigationSelected;
        }

        private void deleteJudicialLitigation(int? judicialLitigationId, LitigationLogic litigationLogic)
        {
            juditial_litigation judicialLitigation = getJudicialLitigationByJudicialLitigationId(judicialLitigationId, litigationLogic);
            IConstants.db.Entry(judicialLitigation).State = System.Data.Entity.EntityState.Deleted;
            IConstants.db.SaveChanges();
        }

        private List<state> loadJudicialStatesTheirOwnDropDown()
        {
            List<state> state = new List<state>();

            state firstItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.firstJudicialState))
                .Select(s => s).FirstOrDefault();
            state secondItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.secondJudicialState))
                .Select(s => s).FirstOrDefault();
            state thirdItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.thirdJudicialState))
                .Select(s => s).FirstOrDefault();
            state fourthItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.fourthJudicialState))
                .Select(s => s).FirstOrDefault();
            state fifthItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.fifthJudicialState))
                .Select(s => s).FirstOrDefault();
            state sixthItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.sixthJudicialState))
                .Select(s => s).FirstOrDefault();
            state seventhItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.seventhJudicialState))
                .Select(s => s).FirstOrDefault();

            state.Add(firstItemState);
            state.Add(secondItemState);
            state.Add(thirdItemState);
            state.Add(fourthItemState);
            state.Add(fifthItemState);
            state.Add(sixthItemState);
            state.Add(seventhItemState);
            return state;
        }

        private List<state> loadJudicialCautionStatesTheirOwnDropDown()
        {
            List<state> state = new List<state>();

            state firstItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.eighthJudicialState))
                .Select(s => s).FirstOrDefault();
            state secondItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.ninethJudicialState))
                .Select(s => s).FirstOrDefault();
            state thirdItemState = IConstants.db.state.Where(s => s.State1.Equals(IConstants.tenthJudicialState))
                .Select(s => s).FirstOrDefault();

            state.Add(firstItemState);
            state.Add(secondItemState);
            state.Add(thirdItemState);
            return state;
        }

        private List<juditial_litigation> getListOfJudicialLitigation()
        {
            List<juditial_litigation> judicialLitigations = new List<juditial_litigation>();
            IConstants.db.client_litigation
                .ToList()
                .ForEach(cl => {
                    cl.litigation.juditial_litigation.ToList().ForEach(lj => {
                        judicialLitigations.Add(new juditial_litigation {
                            id = lj.id,
                            Actor = lj.Actor,
                            Defendant = lj.Defendant,
                            Adjuvant = lj.Adjuvant,
                            Court = lj.Court,
                            litigation = lj.litigation,
                            idLitigation = lj.idLitigation
                        });
                    });
                });
            return judicialLitigations;
        }

        private List<juditial_litigation> getJudicialLitigationByClientId(int? clientId)
        {
            List<juditial_litigation> judicialLitigations = new List<juditial_litigation>();
            IConstants.db.client_litigation
                .Where(cl => cl.idClient == clientId)
                .ToList()
                .ForEach(cl => {
                    cl.litigation.juditial_litigation.ToList().ForEach(lj => {
                        judicialLitigations.Add(new juditial_litigation {
                            id = lj.id,
                            Actor = lj.Actor,
                            Defendant = lj.Defendant,
                            Adjuvant = lj.Adjuvant,
                            Court = lj.Court,
                            litigation = lj.litigation,
                            idLitigation = lj.idLitigation
                        });
                    });
                });
            return judicialLitigations;
        }
    }
}
