﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitigationICS.DAL;
using ICS.Litigios.Entities;

namespace LitigationICS.BLL
{
    public class LegalRepresentativeLogic
    {
        public LegalRepresentativeLogic()
        {
        }

        public int registerNewLegalRepresentative(int? personId)
        {
            int lastLegalRepresentative = 0;
            legalrepresentative legalRepresentative = new legalrepresentative{
                idPerson = personId
            };
            IConstants.db.legalrepresentative.Add(legalRepresentative);
            IConstants.db.SaveChanges();
            lastLegalRepresentative = saveLegalRepresentative(legalRepresentative);
            return lastLegalRepresentative;
        }

        public int saveLegalRepresentative(legalrepresentative legalRepresentative) 
        {
            IConstants.db.SaveChanges();
            return legalRepresentative.id;
        }
    }
}
