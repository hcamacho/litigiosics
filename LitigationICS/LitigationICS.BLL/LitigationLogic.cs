﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LitigationICS.DAL;
using ICS.Litigios.Entities;

namespace LitigationICS.BLL
{
    public class LitigationLogic
    {
        private JudicialLitigationLogic judicialLitigationLogic;
        private AdminLitigationLogic adminLitigationLogic;

        public LitigationLogic()
        {
            judicialLitigationLogic = new JudicialLitigationLogic();
            adminLitigationLogic = new AdminLitigationLogic();
        }

        public bool exitSomeClientWithLitigations() 
        {
            bool exit = false;

            try {
                client client = IConstants.db.client_litigation.Select(k => k).FirstOrDefault().client;
                if(client != null)
                    exit = true;
            }
            catch {
                exit = false; 
            }
            return exit;
        }

        public int? getJudicialOrAdminLitigationId(int type, int? litigationId) 
        {
            int? judicialOrAdminLitigationId = 0;

            if (type == 1){
                judicialOrAdminLitigationId = judicialLitigationLogic.getJudicialLitigationByLitigationId(litigationId).id;
            }
            else {
                judicialOrAdminLitigationId = adminLitigationLogic.getAdminLitigationByLitigationId(litigationId).id;
            }
            return judicialOrAdminLitigationId;
        }

        public byte? getLitigationTypeByLitigationId(int? litigationId) 
        {
            byte? litigationType = IConstants.db.litigation
                .Where(k => k.id == litigationId)
                .Select(litigation => litigation).FirstOrDefault().type;
            return litigationType;

        }

        public litigation findLitigationById(int? litigationId) 
        {
            litigation _Litigation = IConstants.db.litigation.Find(litigationId);
            return _Litigation;
        }

        public List<litigation> getListOfLitigationsWithClientByClientId(int? clientId) 
        {
            List<litigation> listLitigations = new List<litigation>();
            IConstants.db.client_litigation
                .Where(cl => cl.client.id == clientId)
                .ToList()
                .ForEach(l => {
                    listLitigations.Add(new litigation  {
                        id = l.litigation.id,
                        recordNumber = l.litigation.recordNumber,
                        type = l.litigation.type,
                        objectProcess = l.litigation.objectProcess,
                        initDate = l.litigation.initDate,
                        actualState = l.litigation.actualState,
                        nextStepDetail = l.litigation.nextStepDetail,
                        nextStepDate = l.litigation.nextStepDate
                    });
                });
            return listLitigations;
        }

        public int? getLitigationId(int? judicialLitigationId, int? adminLitigationId) 
        {
            juditial_litigation judicialLitigation = null;
            admin_litigation adminLitigation = null;
            int? litigationId = 0;
            if (judicialLitigationId != 0) {
                judicialLitigation = IConstants.db.juditial_litigation.Where(myJudicialLitigation =>
                            myJudicialLitigation.id == judicialLitigationId).FirstOrDefault();
                litigationId = judicialLitigation.idLitigation;
            }
            else{
                adminLitigation = IConstants.db.admin_litigation.Where(myAdminLitigation =>
                            myAdminLitigation.id == adminLitigationId).FirstOrDefault();
                litigationId = adminLitigation.idLitigation;
            }
            return litigationId;
        }

        public int registerNewJudicialLitigation(juditial_litigation judicialLitigation, int clientId) 
        {
            int lastLitigationId = registerLitigation(judicialLitigation);
            registerClientLitigation(clientId, lastLitigationId);
            int lastJudicialLitigationId = registerJudicialLitigation(judicialLitigation, lastLitigationId);
            return lastJudicialLitigationId;
        }

        public int registerNewAdminLitigation(admin_litigation adminLitigation, int clientId)
        {
            int lastLitigationId = registerLitigation(adminLitigation);
            registerClientLitigation(clientId, lastLitigationId);
            int lastJudicialLitigationId = registerAdminLitigation(adminLitigation, lastLitigationId);
            return lastJudicialLitigationId;
        }

        public SelectList getLitigationTypeOnDropDown(string stateName)
        {
            return new SelectList(IConstants.db.type, IConstants.Type, IConstants.Type, stateName);
        }

        public bool getDeleteLitigation(int? litigationId)
        {
            try{
                deleteLitigation(litigationId);
                return true;
            }
            catch{
                return false;
            }
        }

        public bool getDeleteClientLitigation(int? litigationId) 
        {
            try{
                deleteClientLitigation(litigationId);
                return true;
            }
            catch {
                return false;
            }
        }

        public void updateLitigationByObject(admin_litigation aLitigation, juditial_litigation jLitigation, bool? litigationType)
        {
            if (litigationType == true){
                IConstants.db.Entry(jLitigation);
                IConstants.db.SaveChanges();
            }
            else{
                IConstants.db.Entry(aLitigation);
                IConstants.db.SaveChanges();
            }
        }

        public long? getEstimate(bool? unrated, long? estimate)
        {
            if (unrated.Value)
                return null;
            return setEstimateValue(estimate);
        }

        public bool? getUnrated(bool? unrated)
        {
            if (unrated.Value)
                return unrated.Value;
            return false;
        }

        public long? setEstimateValue(long? estimate)
        {
            if (isEstimateNull(estimate))
            {
                estimate = 0;
            }
            return estimate;
        }

        private bool isEstimateNull(long? estimate)
        {
            if (!String.IsNullOrEmpty(estimate.ToString())) {
                return false;
            }
            return true;
        }

        private void deleteLitigation(int? litigationId)
        {
            litigation litigation = findLitigationById(litigationId);
            IConstants.db.Entry(litigation).State = System.Data.Entity.EntityState.Deleted;
            IConstants.db.SaveChanges();
        }

        private void deleteClientLitigation(int? litigationId) 
        {
            client_litigation clientLitigation = IConstants.db.client_litigation.SingleOrDefault(cl => cl.idLitigation == litigationId);
            IConstants.db.Entry(clientLitigation).State = System.Data.Entity.EntityState.Deleted;
            IConstants.db.SaveChanges();
        }

        private int registerJudicialLitigation(juditial_litigation judicialLitigation, int lastLitigationId)
        {
            juditial_litigation newJudicialLitigation = judicialLitigation;
            IConstants.db.juditial_litigation.Add(newJudicialLitigation);
            IConstants.db.SaveChanges();
            int lastJudicialLitigationId = newJudicialLitigation.id;
            return lastJudicialLitigationId;
        }

        private void registerClientLitigation(int clientId, int lastLitigationId)
        {
            client_litigation newClientLitigation = new client_litigation{
                idClient = clientId,
                idLitigation = lastLitigationId
            };
            IConstants.db.client_litigation.Add(newClientLitigation);
            IConstants.db.SaveChanges();
        }

        private int registerLitigation(juditial_litigation judicialLitigation)
        {
            litigation newLitigation = judicialLitigation.litigation;
            IConstants.db.litigation.Add(newLitigation);
            IConstants.db.SaveChanges();
            int lastLitigationId = newLitigation.id;
            return lastLitigationId;
        }

        private int registerLitigation(admin_litigation adminLitigation)
        {
            litigation newLitigation = adminLitigation.litigation;
            IConstants.db.litigation.Add(newLitigation);
            IConstants.db.SaveChanges();
            int lastLitigationId = newLitigation.id;
            return lastLitigationId;
        }

        private int registerAdminLitigation(admin_litigation adminLitigation, int lastLitigationId)
        {
            admin_litigation newAdminLitigation = adminLitigation;
            IConstants.db.admin_litigation.Add(newAdminLitigation);
            IConstants.db.SaveChanges();
            int lastJudicialLitigationId = newAdminLitigation.id;
            return lastJudicialLitigationId;
        }
    }
}
