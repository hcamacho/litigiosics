﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitigationICS.DAL;
using ICS.Litigios.Entities;

namespace LitigationICS.BLL
{
    public class ObservationLogic
    {
        public ObservationLogic() 
        {
        }

        public int registerNewObservation(string subject, DateTime? date, string body, int? litigationId)
        {
            int lastObservationRegister = 0;
            observation observation = new observation {
                Subject = subject,
                Date = date,
                Body = body,
                idLitigations = litigationId
            };
            IConstants.db.observation.Add(observation);
            lastObservationRegister = registerObservation(observation);
            return lastObservationRegister;
        }

        public observation getObservationById(int? observationId) 
        {
            observation observation = IConstants.db.observation
                .Where(observations => observations.id == observationId).FirstOrDefault();
            return observation;
        }

        public List<observation> getListOfObservationsFromJudicialLitigation(int? pJudicialLitigation) 
        {
            List<observation> listObservations = getObservationsFromJudicialLitigation(pJudicialLitigation);
            return listObservations;
        }

        public List<observation> getListOfObservationsFromAdminLitigation(int? adminLitigation)
        {
            List<observation> listObservations = getAdminLitigationDataFromObservations(adminLitigation);
            return listObservations;
        }

        public observation findObservationById(int? observationId) 
        {
            observation _Observation = IConstants.db.observation.Find(observationId);
            return _Observation;
        }

        public void updateObservation(observation observationToUpdate) 
        {
            IConstants.db.Entry(observationToUpdate);
            IConstants.db.SaveChanges();
        }

        private List<observation> getObservationsFromJudicialLitigation(int? judicialLitigation)
        {
            List<observation> listObservations = new List<observation>();
            IConstants.db.juditial_litigation
                .Where(jl => jl.id == judicialLitigation)
                .ToList()
                .ForEach(cl => {
                    cl.litigation.observation.ToList().ForEach(ob => {
                        listObservations.Add(new observation {
                            id = ob.id,
                            Subject = ob.Subject,
                            Body = ob.Body,
                            Date = ob.Date
                        });
                    });
                });
            return listObservations;

        }

        private List<observation> getAdminLitigationDataFromObservations(int? adminLitigation)
        {
            List<observation> listObservations = new List<observation>();
            IConstants.db.admin_litigation
                .Where(al => al.id == adminLitigation)
                .ToList()
                .ForEach(cl => {
                    cl.litigation.observation.ToList().ForEach(ob => {
                        listObservations.Add(new observation {
                            id = ob.id,
                            Subject = ob.Subject,
                            Body = ob.Body,
                            Date = ob.Date
                        });
                    });
                });
            return listObservations;
        }

        private int registerObservation(observation observationInserted)
        {
            IConstants.db.SaveChanges();
            return observationInserted.id;
        }

    }
}
