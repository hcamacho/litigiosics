﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LitigationICS.DAL;

namespace LitigationICS.BLL
{
    public class PersonLogic
    {
        public PersonLogic() 
        {
        }

        public int registerNewPerson(string name, string DNI, string address, string email, string phone) 
        {
            int lastPersonRegistered = registerClientPerson(name, DNI, address,email,phone);
            return lastPersonRegistered;
        }

        public int registerNewContactPerson(string clientName, string contactName, string lastName, string DNI, string address, string email, string phone, bool isForEdit)
        {
            int lastPersonRegistered = registerContactPerson(clientName,contactName, lastName, DNI, address, email, phone, isForEdit);
            return lastPersonRegistered;
        }

        public int registerNewLegalRepresentativePerson(string name, string DNI) 
        {
            int lastPersonRegistered = registerLegalRepresentativePerson(name, DNI);
            return lastPersonRegistered;
        }

        public person updatePersonByObject(person person)
        {
            return updatePerson(person);
        }

        public bool isPersonNameRepeated(string name)
        {
            bool isRepeated = false;
            var personNames = IConstants.db.person.Select(personSelected => personSelected).ToList();
            foreach (person personName in personNames){
                if(personName.Name.Equals(name)){isRepeated = true; break;}
            }
            return isRepeated;
        }

        private person updatePerson(person updatePerson)
        {
            bool isUpdated = false;
            person person = new person {
                Name = updatePerson.Name, 
                DNI = updatePerson.DNI, 
                Address = updatePerson.Address 
            };

            isUpdated = isPersonUpdated(person);
            if (isUpdated){
                return person;
            }
            return null;
        }

        private bool isPersonUpdated(person personUpdated)
        {
           IConstants.db.Entry(personUpdated);
           IConstants.db.SaveChanges();
            return true;
        }

        private int registerClientPerson(string name, string DNI, string address, string email, string phone) 
        {
            int lastPersonRegistered = 0;
            if (!isPersonNameRepeated(name)){
                if(String.IsNullOrEmpty(address)){
                    address = "N/A";
                }
                person person = new person {
                    Name = name,
                    DNI = DNI,
                    Address = address
                };
                IConstants.db.person.Add(person);
                lastPersonRegistered = savePerson(person);
                registerNewContactPhoneAndEmail(email, phone, lastPersonRegistered);
                if(lastPersonRegistered > 0){
                    return lastPersonRegistered;
                }
            }
            return lastPersonRegistered;
        }

        private int registerLegalRepresentativePerson(string name, string DNI)
        {
            int lastPersonRegistered = 0;
            person person = new person {
                Name = name,
                LastName = null,
                DNI = DNI,
                Address = null
            };
            IConstants.db.person.Add(person);
            lastPersonRegistered = savePerson(person);
            if (lastPersonRegistered > 0)
                return lastPersonRegistered;
            return lastPersonRegistered;
        }

        private int registerContactPerson(string clientName, string contactName, string lastName, string DNI, string address, string email, string phone, bool editContact)
        {
            int lastPersonRegistered = 0;

            if (editContact){
                if (!isPersonNameRepeated(clientName)){
                    if (String.IsNullOrEmpty(address))
                        address = "N/A";
                    if (String.IsNullOrEmpty(lastName))
                        lastName = "N/A";
                    person person = new person {
                        Name = contactName,
                        LastName = lastName
                    };

                    IConstants.db.person.Add(person);
                    lastPersonRegistered = savePerson(person);
                    registerNewContactPhoneAndEmail(email, phone, lastPersonRegistered);
                    return lastPersonRegistered;
                }
                return lastPersonRegistered;
            }
            else {
                if (String.IsNullOrEmpty(address))
                    address = "N/A";
                if (String.IsNullOrEmpty(lastName))
                    lastName = "N/A";
                person person = new person{
                    Name = contactName,
                    LastName = lastName,
                    DNI = DNI,
                    Address = address
                };
                IConstants.db.person.Add(person);
                lastPersonRegistered = savePerson(person);
                registerNewContactPhoneAndEmail(email, phone, lastPersonRegistered);
                return lastPersonRegistered;
            }
        }

        private void registerNewContactPhoneAndEmail(string email, string phone, int lastPersonRegistered)
        {
            if (!String.IsNullOrEmpty(email)){
                user_email user_mail = new user_email { 
                    Email = email, 
                    idUser = lastPersonRegistered 
                };

                IConstants.db.user_email.Add(user_mail);
                saveUserEmail(user_mail);
            }
            if (phone != null){
                user_phone user_phone = new user_phone { 
                    Phone = phone, 
                    idUser = lastPersonRegistered 
                };

                IConstants.db.user_phone.Add(user_phone);
                saveUserPhone(user_phone);
            }
        }

        public void updateNewContactPhoneAndEmail(string email, string phone, int lastPersonRegistered, user_email user_email, user_phone user_phone)
        {
            if (!String.IsNullOrEmpty(email)){
                user_email = new user_email{
                    Email = email,
                    idUser = lastPersonRegistered
                };

                IConstants.db.user_email.Add(user_email);
                saveUserEmail(user_email);
            }
            if (phone != null){
                user_phone = new user_phone{
                    Phone = phone,
                    idUser = lastPersonRegistered
                };

                IConstants.db.user_phone.Add(user_phone);
                saveUserPhone(user_phone);
            }
        }

        private int savePerson(person personInserted)
        {
            IConstants.db.SaveChanges();
            return personInserted.id;
        }

        private void saveUserEmail(user_email emailInserted)
        {
            IConstants.db.SaveChanges();
        }

        private void saveUserPhone(user_phone phoneInserted)
        {
            IConstants.db.SaveChanges();
        }

    }
}
