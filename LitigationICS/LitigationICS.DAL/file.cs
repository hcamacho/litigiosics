//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LitigationICS.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class file
    {
        public file()
        {
            this.document_observation = new HashSet<document_observation>();
        }
    
        public int id { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        public string DestinationFile { get; set; }
    
        public virtual ICollection<document_observation> document_observation { get; set; }
    }
}
