﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using LitigationICS.BLL;
using ICS.Litigios.Entities;

namespace LitigationICS.Test
{
    [TestClass]
    public class GetDataFromDB
    {
        private ClientLogic clientLogic;
        private LitigationLogic litigationLogic;
        private ObservationLogic observationLogic;

        public GetDataFromDB() 
        {
            clientLogic = new ClientLogic();
            litigationLogic = new LitigationLogic();
            observationLogic = new ObservationLogic();
        }

        [TestMethod]
        public void testClients() 
        {
            int? clientId = clientLogic.getLastClientIdRegistratedWithLitigation();
            Assert.IsTrue(clientId != 0);
        }

        [TestMethod]
        public void testLitigations()
        {
            //TODO: CREATE TEST AGAIN FOR LITIGATIONS
        }

        [TestMethod]
        public void testObservations() 
        {
            //TODO: CREATE TEST AGAIN FOR CLIENTS
        }
    }
}
