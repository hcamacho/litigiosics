# Litigios ICS

Litigios ICS is a project made in ASP.NET MVC 5, for managing legal processes

## Getting Started

### Prerequisites

A bitbucket account is needed, if one doesn't have an account, it can be created for free at:
[https://bitbucket.org](https://bitbucket.org/account/signup/)

To clone the repository, `git` is needed, for windows, it can be obtained for free at:
[https://git-scm.com/](https://git-scm.com/download/win)

during the installation, it's recommended to choose the following options:

* `use git bash only`
* `checkout as-is, commit Unix style endings`

Visual Studio is required (Minimum VS2013 Community Edition), it can be obtained for free at:
[https://www.visualstudio.com/](https://www.visualstudio.com/downloads/) 
(At the time of writing of this README, Visual Studio Community 2017 is being used)

MS SQL Server is required (Minimum 2012), it can be obtained for free at: 
[https://www.microsoft.com/en-us/sql-server/sql-server-downloads](https://www.microsoft.com/en-us/sql-server/sql-server-downloads)
(At the time of writing this README, MS SQL Server 2016 Developer Edition is being used)

The SQL Server instance must be installed with mixed authentication mode (SQL Server & Windows authentication) to be able to use the current deployment configuration with minimal changes

![IMPORTANT!](doc/images/sql_server_important_installation_step.png  "Important Installation Step")

MS SQL Server Management Studio is required (verify that it works with the version of MS SQL Server being used), it can be obtained for free at:
[https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms)

After Installing the required software, the repository can be cloned with the command:

```
git clone git@bitbucket.org:hcamacho/litigiosics.git
```

or, if using https, after obtaining access to the repository:

```
git clone https://<username>@bitbucket.org/hcamacho/litigiosics.git
```
where `<username>` is one's Bitbucket username

#### Recommendations

It's recommended to install a font that is apt to be used in a command line interface (i.e. a Monospaced font), such as [Terminus](http://files.ax86.net/terminus-ttf/files/latest.zip)

and to setup git bash to use Terminus Size 12px, and disable `anti-aliasing`

and additionally, to install the [Solarized](http://ethanschoonover.com/solarized) theme, and to optionally set up the Terminus font, or another Monospaced font to edit source code

### Set Up

After installing the required software and cloning the repository

one must use the SQL script to get a working copy of the DataBase in the local machine for either testing or development, it's found on the `doc/db` directory, in the same directory as this README

to restore it in MS SQL Server Management Studio, open the SQL file, and adjust the paths to where the `.BAK` file is located in the local machine, as well as adjusting the path of the MS SQL Server Instance

### Code style-guide

It's recommended to read (or at the very least, take a look at) the existing code in the DAL, BLL, or WebApp, and to keep all added code consistent with it.

## Authors

* **David Cordero** - First Revision of this README
