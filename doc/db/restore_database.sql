RESTORE DATABASE LitigiosICS
FROM DISK = 'E:\pernix\litigiosics\LitigiosICS_15122016.BAK'
WITH
MOVE 'LitigiosICS' TO 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\LitigationICS.mdf',
MOVE 'LitigiosICS_LOG' TO 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\LitigationICS.ldf'